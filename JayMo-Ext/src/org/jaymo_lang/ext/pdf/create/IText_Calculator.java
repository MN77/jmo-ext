/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.pdf.create;

import org.jaymo_lang.ext.pdf.create.page.I_IText_PageData;
import org.jaymo_lang.object.immute.JMo_Enum;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 30.01.2022
 */
public class IText_Calculator {

	private boolean anchorTop = true; // Default anchor of iText is bottom-left, but better to use is top-left
	private float   metrics   = 1f;


	public float height( final float height ) {
		return height / this.metrics;
	}

	public float o( final float o ) {
		return this.iMetrics( o );
	}

	public Float reverse( final int i ) {
		return i / this.metrics;
	}

	public void setAnchor( final boolean top ) {
		this.anchorTop = top;
	}

	public void setMetrics( final JMo_Enum arg ) {

//		return (float)(cm * 28.25); //in der Breite wärens eigentlich 28.18 oder so!   28.25 ist in der Höhe ok
//		return (float)(cm / 2.54 * 72); //Offizielle Umrechnung! Entspricht also 28.346! Ist nur leider zu groß
		switch( arg.getName() ) {
			case "ORIGINAL":
				this.metrics = 1f;
				break;
			case "ZOLL":
				this.metrics = 72f; // = 1*72
				break;
			case "CM":
				this.metrics = 28.346456693f; // = 1/2.54*72
				break;
			case "MM":
				this.metrics = 283.464566929f; // = 1/2.54*72*10
				break;
			default:
				throw Err.invalid( arg.getName() );
		}
	}

	public float width( final float width ) {
		return width / this.metrics;
	}

	public float x( final float x ) {
		return this.iMetrics( x );
	}

	public float y( final I_IText_PageData page, final float y ) {
		float value = this.iMetrics( y );
		if( this.anchorTop )
			value = page.getHeight() - value;
		return value;
	}

	private float iMetrics( final float f ) {
		return f * this.metrics;
	}

}
