/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.pdf.create;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.ext.pdf.create.page.JMo_IText_Page;
import org.jaymo_lang.ext.pdf.create.text.I_IText_TextItem;
import org.jaymo_lang.ext.pdf.create.text.JMo_IText_TextChunk;
import org.jaymo_lang.ext.pdf.create.text.JMo_IText_TextParagraph;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.JMo_Enum;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.runtime.CallRuntime;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfWriter;

import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @see New and free Alternative: PDF-Box https://pdfbox.apache.org/    https://coderanch.com/wiki/659953/PDFBox
 */
public class JMo_IText_Document extends A_ObjectSimple {

//	private OutputStream stream;
	private final ArrayList<I_IText_BaseItem> items = new ArrayList<>();
	private final IText_Calculator            calc  = new IText_Calculator();


//	public JMo_IText_Document(final String pdfFile) throws Err_FileSys {
//		Err.ifNull(pdfFile);
//		try {
//			this.stream = new FileOutputStream(pdfFile);
//		}
//		catch(final FileNotFoundException e) {
//			Err.wrap(e, pdfFile);
//		}
//		this.items = new MList<>();
//	}
//
//	public JMo_IText_Document(final File pdfFile) throws Err_FileSys {
//		Err.ifNull(pdfFile);
//		try {
//			this.stream = new FileOutputStream(pdfFile);
//		}
//		catch(final FileNotFoundException e) {
//			throw Err.wrap(e, "File not found");
//		}
//		this.items = new MList<>();
//	}
//
//	public JMo_IText_Document(final I_File pdfFile) throws Err_FileSys {
//		Err.ifNull(pdfFile);
//		this.stream = pdfFile.write();
//		this.items = new MList<>();
//	}


	/**
	 * °ORIGINAL()Enum # Constant for original metrics
	 * °CM()Enum # Constant for centimeter metrics
	 * °MM()Enum # Constant for millimeter metrics
	 * °ZOLL()Enum # Constant for zoll metrics
	 */
	@Override
	public A_Immutable getConstant( final CallRuntime cr, final String name ) {

		switch( name ) {
			case "ZOLL":
				return new JMo_Enum( this.getTypeName(), name );
			case "ORIGINAL":
				return new JMo_Enum( this.getTypeName(), name );
			case "CM":
				return new JMo_Enum( this.getTypeName(), name );
			case "MM":
				return new JMo_Enum( this.getTypeName(), name );
		}
		return null;
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "newPage":
				return this.mNewPage( cr );

			case "newParagraph":
				return this.mNewParagraph( cr );
			case "newText": // TODO wirklich okay?!?
			case "newTextChunk":
				return this.mNewTextChunk( cr );

			case "write":
				this.mWrite( cr );
				return this;

			case "setAnchor":
				this.mSetAnchor( cr );
				return this;
			case "setMetrics":
				this.mSetMetrics( cr );
				return this;
		}
		return null;
	}

//	public S_PDF_FT_Gruppe newTextGroup(String text)   { //TODO Aktuelle Formatvorgaben übergeben
//		cr.argsNone();
//		S_PDF_FT_Gruppe result = new PDF_FT_Gruppe(text);
//		this.items.add(result);
//		return result;
//	}

	// TODO Phrase

	private void iWrite( final CallRuntime cr, final OutputStream stream ) {
		Document document = null;
		PdfWriter writer = null;

		for( final I_IText_BaseItem item : this.items )
			if( item instanceof final JMo_IText_Page page ) {
				Rectangle rect = page.getFormat();
				if( page.isLandscape() )
					rect = rect.rotate();

				try {

					if( document == null ) {
						document = new Document( page.getFormat() );
						writer = PdfWriter.getInstance( document, stream );
						writer.setPdfVersion( PdfWriter.VERSION_1_2 ); // Default = 1.2 (1.6 is necessary Acrobat >= 6)

						// Does nothing on pages:
//						writer.setMargins(0, 0, 0, 0);
//						writer.setDuration(int arg0);
//						writer.setUserunit(0);
//						writer.setViewerPreferences(0);

						document.open();
					}
					else {
						document.setPageSize( rect );
						document.newPage();
						writer.setPageEmpty( false );
					}
					document.add( new Chunk( "" ) ); // Necessary for background color
					page.draw( cr, writer.getDirectContent() );
				}
				catch( final DocumentException e ) {
					throw Err.direct( "PDF-Error", e );
				}
			}
			else
				((I_IText_TextItem)item).draw( document );

		document.close();
	}

	/**
	 * °newPage()IText_Page # Create a new page
	 */
	private JMo_IText_Page mNewPage( final CallRuntime cr ) { //TODO Aktuelle Formatvorgaben übergeben
		cr.argsNone();
		final JMo_IText_Page result = new JMo_IText_Page( this.calc );
		this.items.add( result );
		return result;
	}

	/**
	 * °newParagraph(Str text)IText_TextParagraph # Create a new text paragraph
	 */
	private JMo_IText_TextParagraph mNewParagraph( final CallRuntime cr ) { //TODO Aktuelle Formatvorgaben übergeben
		final JMo_Str arg0 = cr.arg( this, JMo_Str.class );
		final String text = arg0.rawString();
		final JMo_IText_TextParagraph result = new JMo_IText_TextParagraph( text );
		this.items.add( result );
		return result;
	}

	/**
	 * °newText ^ newTextChunk
	 * °newTextChunk(Str text)IText_TextChunk # Create a new text chunk
	 */
	private JMo_IText_TextChunk mNewTextChunk( final CallRuntime cr ) { //TODO Aktuelle Formatvorgaben übergeben
		final JMo_Str arg0 = cr.arg( this, JMo_Str.class );
		final String text = arg0.rawString();
		final JMo_IText_TextChunk result = new JMo_IText_TextChunk( text );
		this.items.add( result );
		return result;
	}

	/**
	 * °setAnchor(MagicPosition anchor)Same # Set the anchor (TOP_LEFT, BOTTOM_LEFT) which should be used.
	 */
	private void mSetAnchor( final CallRuntime cr ) {
		final MagicPosition arg = (MagicPosition)cr.args( this, MagicPosition.class )[0];
		final POSITION p = arg.get();
		if( p != POSITION.TOP_LEFT && p != POSITION.BOTTOM_LEFT )
			throw new RuntimeError( cr, "Invalid anchor", "Anchor must be Top-Left or Bottom-Left, but got: " + p.toString() );	//ok
		this.calc.setAnchor( p == POSITION.TOP_LEFT );
	}

	/**
	 * °setMetrics(Enum metrics)Same # Set the metrics (ORIGINAL,CM,MM,ZOLL) which should be used.
	 */
	private void mSetMetrics( final CallRuntime cr ) {
		final JMo_Enum arg = cr.argEnum( this );
		this.calc.setMetrics( arg );
	}

	/**
	 * °write(File file)Same # Write the PDF to a file.
	 * °write(Str file)Same # Write the PDF to a file.
	 */
	private void mWrite( final CallRuntime cr ) {
		final I_Object arg = cr.argExt( this, JMo_File.class, JMo_Str.class );

		if( arg instanceof JMo_Str argStr ) {
			final String file = argStr.rawString();

			try {
				final OutputStream stream = new FileOutputStream( file );
				this.iWrite( cr, stream );
			}
			catch( final FileNotFoundException e ) {
				throw new ExternalError( cr, "PDF write error", "Could not write to file: " + file );
			}
		}
		else { // JMo_File
			final JMo_File file = (JMo_File)arg;

			try {
				final OutputStream stream = new FileOutputStream( file.getInternalFile() );
				this.iWrite( cr, stream );
			}
			catch( final FileNotFoundException e ) {
				throw new ExternalError( cr, "PDF write error", "Could not write to file: " + file.getInternalFile().getAbsolutePath() );
			}
		}
	}

}
