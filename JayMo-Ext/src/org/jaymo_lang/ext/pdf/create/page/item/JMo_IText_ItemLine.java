/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.pdf.create.page.item;

import java.awt.Color;

import org.jaymo_lang.ext.pdf.create.IText_Calculator;
import org.jaymo_lang.ext.pdf.create.page.I_IText_PageData;
import org.jaymo_lang.lib.graphic.Util_Color;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Number;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;

import com.lowagie.text.pdf.PdfContentByte;


/**
 * @author Michael Nitsche
 */
public class JMo_IText_ItemLine extends A_ObjectSimple implements I_IText_PageItem {

	private int[]       colorRGB;
	private Float       thickness = null;
	private final float x1, x2, y1, y2;


	public JMo_IText_ItemLine( final float x1, final float y1, final float x2, final float y2, final Float defaultLineThickness, final int[] defaultColorLine ) {
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		this.thickness = defaultLineThickness;
		this.colorRGB = defaultColorLine != null ? defaultColorLine : new int[]{ 0, 0, 0 };
	}

	public void draw( final CallRuntime cr, final I_IText_PageData page, final PdfContentByte cb, final IText_Calculator calc ) {
		if( this.thickness == null )
			this.thickness = calc.reverse( 1 );

		cb.setColorStroke( new Color( this.colorRGB[0], this.colorRGB[1], this.colorRGB[2] ) );
		cb.setLineWidth( calc.o( this.thickness ) );
		cb.moveTo( calc.x( this.x1 ), calc.y( page, this.y1 ) );
		cb.lineTo( calc.x( this.x2 ), calc.y( page, this.y2 ) );
		cb.stroke();
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "setThickness":
				return this.mSetThickness( cr );
			case "setColor":
				return this.mSetColor( cr );
		}
		return null;
	}

	/**
	 * °setColor(Color color)Same # Set the color for this line
	 * °setColor(Int r, Int g, Int b)Same # Set the color for this line
	 */
	private JMo_IText_ItemLine mSetColor( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 3 );
		this.colorRGB = Util_Color.argsToRGB( cr, args );
		return this;
	}

	/**
	 * °setThickness(Number thickness)Same # Set the thickness of this line
	 */
	private JMo_IText_ItemLine mSetThickness( final CallRuntime cr ) {
		final I_Number arg = (I_Number)cr.args( this, I_Number.class )[0];
		final float f = Lib_Convert.toFloat( cr, arg );
		Lib_Error.ifNotBetween( cr, 0, 50, f, "thickness" );
		this.thickness = f;
		return this;
	}

}
