/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.pdf.create.page;

import java.util.ArrayList;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.ext.pdf.create.IText_Calculator;
import org.jaymo_lang.ext.pdf.create.I_IText_BaseItem;
import org.jaymo_lang.ext.pdf.create.constant.JMo_IText_PageSize;
import org.jaymo_lang.ext.pdf.create.page.item.I_IText_PageItem;
import org.jaymo_lang.ext.pdf.create.page.item.JMo_IText_ItemCircle;
import org.jaymo_lang.ext.pdf.create.page.item.JMo_IText_ItemImage;
import org.jaymo_lang.ext.pdf.create.page.item.JMo_IText_ItemLine;
import org.jaymo_lang.ext.pdf.create.page.item.JMo_IText_ItemRectangle;
import org.jaymo_lang.ext.pdf.create.page.item.JMo_IText_ItemText;
import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.graphic.Util_Color;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Number;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;

import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;

import de.mn77.base.data.constant.position.Lib_Position;
import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.data.constant.position.POSITION_H;


/**
 * @author Michael Nitsche
 */
public class JMo_IText_Page extends A_ObjectSimple implements I_IText_BaseItem, I_IText_PageData {

	private int[]                             defaultColorLine     = null, defaultColorFont = null, defaultColorFill = null;
	private String                            defaultFont;
	private POSITION_H                        defaultFontAlign;
	private Integer                           defaultFontSize      = null;
	private Float                             defaultLineThickness = null;
	private Rectangle                         format;
	private boolean                           isLandscape;
	private final ArrayList<I_IText_PageItem> items;
	private final IText_Calculator            calc;


	public JMo_IText_Page( final IText_Calculator calc ) {
		this.items = new ArrayList<>();
		this.isLandscape = false;
		this.format = JMo_IText_PageSize.DEFAULT;
		this.calc = calc;
	}

	public void draw( final CallRuntime cr, final PdfContentByte cb ) {
//		cb.setHorizontalScaling(50f); // Skaliert nur den Text
		Rectangle rect = this.format;
		if( this.isLandscape )
			rect = rect.rotate();
		for( final I_IText_PageItem e : this.items )
			e.draw( cr, this, cb, this.calc );
	}

	public Rectangle getFormat() {
		return this.format;
	}

	public float getHeight() {
		Rectangle format = this.format;
		if( this.isLandscape )
			format = format.rotate();
		return format.getHeight();
	}

	public float getWidth() {
		Rectangle rect = this.format;
		if( this.isLandscape )
			rect = rect.rotate();
		return rect.getWidth();
	}

	public boolean isLandscape() {
		return this.isLandscape;
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "newRectangle":
				return this.mNewRectangle( cr );
			case "newLine":
				return this.mNewLine( cr );
			case "newText":
				return this.mNewText( cr );
			case "newCircle":
				return this.mNewCircle( cr );
			case "newImage":
				return this.mNewImage( cr );

			case "width":
				return this.mWidth( cr );
			case "height":
				return this.mHeight( cr );

			case "setFormat":
				return this.mSetFormat( cr );
			case "setOrientation":
				return this.mSetOrientation( cr );

			case "setDefaultFont":
				return this.mSetDefaultFont( cr );
			case "setDefaultFontsize":
			case "setDefaultFontSize":
				return this.mSetDefaultFontSize( cr );

			case "setDefaultThickness":
				return this.mSetDefaultThickness( cr );
			case "setDefaultFontColor":
			case "setDefaultColorFont":
				return this.mSetDefaultColorFont( cr );
			case "setDefaultFontAlign":
				return this.mSetDefaultFontAlign( cr );

			case "setDefaultLineColor":
			case "setDefaultColorLine":
				return this.mSetDefaultColorLine( cr );
			case "setDefaultFillColor":
			case "setDefaultColorFill":
				return this.mSetDefaultColorFill( cr );
		}
		return null;
	}

	/**
	 * °height()Dec # Returns the height of this page
	 */
	private JMo_Dec mHeight( final CallRuntime cr ) {
		cr.argsNone();
		float f = this.getHeight();
		f = this.calc.width( f );
		return JMo_Dec.valueOf( cr, f );
	}

	/**
	 * °newCircle(Number x, Number y, Number r)IText_ItemCircle # Create a new circle for this page.
	 */
	private JMo_IText_ItemCircle mNewCircle( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_Number.class, I_Number.class, I_Number.class );
		final float x = Lib_Convert.toFloat( cr, args[0] );
		final float y = Lib_Convert.toFloat( cr, args[1] );
		final float r = Lib_Convert.toFloat( cr, args[2] );

		final JMo_IText_ItemCircle result = new JMo_IText_ItemCircle( x, y, r, this.defaultLineThickness, this.defaultColorLine, this.defaultColorFill );
		this.items.add( result );
		return result;
	}

	/**
	 * °newImage(Image image, Number x, Number y)IText_ItemImage # Create a new image on this page
	 * °newImage(Image image, Number x, Number y, Int dx, Int dy)IText_ItemImage # Create a new image with the given size on this page
	 */
	private JMo_IText_ItemImage mNewImage( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 3, 5 );
		Lib_Error.ifIs( cr, 4, args.length, "amount of arguments" );
		final boolean has5Args = args.length == 5;

		final JMo_Image image = cr.argType( args[0], JMo_Image.class );
		final float x = Lib_Convert.toFloat( cr, cr.argType( args[1], I_Number.class ) );
		final float y = Lib_Convert.toFloat( cr, cr.argType( args[2], I_Number.class ) );
		final int dx = has5Args ? Lib_Convert.toInt( cr, cr.argType( args[3], JMo_Int.class ) ) : image.internalGet().getWidth();
		final int dy = has5Args ? Lib_Convert.toInt( cr, cr.argType( args[4], JMo_Int.class ) ) : image.internalGet().getHeight();

		final JMo_IText_ItemImage result = new JMo_IText_ItemImage( image.internalGet(), x, y, dx, dy );
		this.items.add( result );
		return result;
	}

	/**
	 * °newLine(Number x1, Number y1, Number x2, Number y2)IText_ItemLine # Create a new line on this page
	 */
	private JMo_IText_ItemLine mNewLine( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_Number.class, I_Number.class, I_Number.class, I_Number.class );
		final float x1 = Lib_Convert.toFloat( cr, args[0] );
		final float y1 = Lib_Convert.toFloat( cr, args[1] );
		final float x2 = Lib_Convert.toFloat( cr, args[2] );
		final float y2 = Lib_Convert.toFloat( cr, args[3] );

		final JMo_IText_ItemLine result = new JMo_IText_ItemLine( x1, y1, x2, y2, this.defaultLineThickness, this.defaultColorLine );
		this.items.add( result );
		return result;
	}

	/**
	 * °newRectangle(Number x1, Number y1, Number x2, Number y2)IText_ItemRectangle # Create a new rectangle on this page
	 */
	private JMo_IText_ItemRectangle mNewRectangle( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_Number.class, I_Number.class, I_Number.class, I_Number.class );
		final float x = Lib_Convert.toFloat( cr, args[0] );
		final float y = Lib_Convert.toFloat( cr, args[1] );
		final float dx = Lib_Convert.toFloat( cr, args[2] );
		final float dy = Lib_Convert.toFloat( cr, args[3] );

		final JMo_IText_ItemRectangle result = new JMo_IText_ItemRectangle( x, y, dx, dy, this.defaultLineThickness, this.defaultColorLine, this.defaultColorFill );
		this.items.add( result );
		return result;
	}

	/**
	 * °newText(Number x, Number y, Str text)IText_ItemText # Create a new text on this page
	 */
	private JMo_IText_ItemText mNewText( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, I_Number.class, I_Number.class, JMo_Str.class );
		final float x = Lib_Convert.toFloat( cr, args[0] );
		final float y = Lib_Convert.toFloat( cr, args[1] );
		final String text = Lib_Convert.toStr( cr, args[2] ).rawString();

		final JMo_IText_ItemText result = new JMo_IText_ItemText( x, y, text, this.defaultFont, this.defaultFontSize, this.defaultColorFont, this.defaultFontAlign );
		this.items.add( result );
		return result;
	}

	/**
	 * °setDefaultFillColor ^ setDefaultColorFill
	 * °setDefaultColorFill(Color color)Same # Set the default fill color for elements on this page
	 * °setDefaultColorFill(Int r, Int g, Int b)Same # Set the default fill color for elements on this page
	 */
	private JMo_IText_Page mSetDefaultColorFill( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 3 );
		this.defaultColorFill = Util_Color.argsToRGB( cr, args );
		return this;
	}

	/**
	 * °setDefaultFontColor ^ setDefaultColorFont
	 * °setDefaultColorFont(Color color)Same # Set the default font color for elements on this page
	 * °setDefaultColorFont(Int r, Int g, Int b)Same # Set the default font color for elements on this page
	 */
	private JMo_IText_Page mSetDefaultColorFont( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 3 );
		this.defaultColorFont = Util_Color.argsToRGB( cr, args );
		return this;
	}

	/**
	 * °setDefaultLineColor ^ setDefaultColorLine
	 * °setDefaultColorLine(Color color)Same # Set the default line color for elements on this page
	 * °setDefaultColorLine(Int r, Int g, Int b)Same # Set the default line color for elements on this page
	 */
	private JMo_IText_Page mSetDefaultColorLine( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 3 );
		this.defaultColorLine = Util_Color.argsToRGB( cr, args );
		return this;
	}

	/**
	 * °setDefaultFont(Str fontname)Same # Set the default font for elements on this page.
	 */
	private JMo_IText_Page mSetDefaultFont( final CallRuntime cr ) {
		final JMo_Str arg = cr.arg( this, JMo_Str.class ); // TODO Auch Font-Element akzeptieren
		final String fontname = arg.rawString();
		this.defaultFont = fontname;
		return this;
	}

	/**
	 * °setDefaultFontAlign(MagicPosition align)Same # Set the default align for text elements on this page.
	 */
	private JMo_IText_Page mSetDefaultFontAlign( final CallRuntime cr ) { // TODO Rename to setDefaultTextAlign!
		final I_Object arg = cr.args( this, MagicPosition.class )[0];
		final POSITION pos = ((MagicPosition)arg).get();
		if( !Lib_Position.isHorizontal( pos ) )
			throw new RuntimeError( cr, "Invalid alignment", "Text alignment must be left, center or right!" );
		this.defaultFontAlign = (POSITION_H)pos;
		return this;
	}

	/**
	 * °setDefaultFontsize ^ setDefaultFontSize
	 * °setDefaultFontSize(Int size)Same # Set the default font size for elements on this page.
	 */
	private JMo_IText_Page mSetDefaultFontSize( final CallRuntime cr ) {
		final JMo_Int arg = cr.arg( this, JMo_Int.class );
		final int size = arg.rawInt( cr );
		Lib_Error.ifNotBetween( cr, 4, 100, size, "fontsize" );
		this.defaultFontSize = size;
		return this;
	}

	/**
	 * °setDefaultThickness(Number thickness)Same # Set the default thickness for elements on this page
	 */
	private JMo_IText_Page mSetDefaultThickness( final CallRuntime cr ) {
		final I_Number arg = cr.arg( this, I_Number.class );
		final float f = Lib_Convert.toFloat( cr, arg );
		Lib_Error.ifNotBetween( cr, 0.1, 20, f, "thickness" );
		this.defaultLineThickness = f;
		return this;
	}

	/**
	 * °setFormat(IText_PageSize format)Same # Set the format of this page
	 */
	private JMo_IText_Page mSetFormat( final CallRuntime cr ) {
		final JMo_IText_PageSize format = cr.arg( this, JMo_IText_PageSize.class ); // TODO PageSize vs. PageFormat !!!
		this.format = format.getRectangle();
		return this;
	}

	/**
	 * °setOrientation(Bool isLandscape)Same # Set the orientation of this page: true = landscape, false = portrait
	 */
	private JMo_IText_Page mSetOrientation( final CallRuntime cr ) {
		final JMo_Bool arg = cr.arg( this, JMo_Bool.class );
		final boolean value = Lib_Convert.toBoolean( cr, arg );
		this.isLandscape = value;
		return this;
	}

	/**
	 * °width()Dec # Returns the width of this page
	 */
	private JMo_Dec mWidth( final CallRuntime cr ) {
		cr.argsNone();
		float f = this.getWidth();
		f = this.calc.width( f );
		return JMo_Dec.valueOf( cr, f );
	}

}
