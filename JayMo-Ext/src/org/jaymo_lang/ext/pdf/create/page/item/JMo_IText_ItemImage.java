/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.pdf.create.page.item;

import java.awt.Point;

import org.jaymo_lang.ext.pdf.create.IText_Calculator;
import org.jaymo_lang.ext.pdf.create.page.I_IText_PageData;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfContentByte;

import de.mn77.base.error.Err;
import de.mn77.lib.graphic.I_Image;
import de.mn77.lib.graphic.Lib_Graphic;


/**
 * @author Michael Nitsche
 */
public class JMo_IText_ItemImage extends A_ObjectSimple implements I_IText_PageItem {

	private final int     dx, dy;
	private final I_Image image;
	private final float   x, y;


	public JMo_IText_ItemImage( final I_Image image, final float x, final float y, final int dx, final int dy ) {
		this.image = image;
		this.x = x;
		this.y = y;
		this.dx = dx;
		this.dy = dy;
	}

	public void draw( final CallRuntime cr, final I_IText_PageData page, final PdfContentByte cb, final IText_Calculator calc ) {

		try {
			final Image b = Image.getInstance( this.image.getImage(), null, false );
			final Point p = Lib_Graphic.calcResize( this.image.getWidth(), this.image.getHeight(), this.dx, this.dy );
			b.scaleAbsolute( calc.o( p.x ), calc.o( p.y ) );
			b.setAbsolutePosition( calc.x( this.x ), calc.y( page, this.y ) - calc.o( p.y ) );
			cb.addImage( b );
		}
		catch( final Exception e ) {
			Err.show( e );
		}
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {
//		switch(method) {}
		return null;
	}

}
