/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.pdf.create.constant;

import java.io.IOException;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.runtime.CallRuntime;

import com.lowagie.text.DocumentException;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class JMo_IText_Font extends A_Immutable {

	private final ArgCallBuffer arg;
	private final String        bold, italic, boldItalic;
	private String              regular;


	/**
	 * +IText_Font()
	 */
	public JMo_IText_Font() {
		this.arg = null;
		this.regular = FontFactory.HELVETICA;
		this.bold = FontFactory.HELVETICA_BOLD;
		this.italic = FontFactory.HELVETICA_OBLIQUE;
		this.boldItalic = FontFactory.HELVETICA_BOLDOBLIQUE;
	}

	/**
	 * +IText_Font(Str fontname)
	 */
	public JMo_IText_Font( final Call fontname ) {
		this.arg = new ArgCallBuffer( 0, fontname );
		this.regular = null;
		this.bold = null;
		this.italic = null;
		this.boldItalic = null;
	}

	public JMo_IText_Font( final String regular, final String bold, final String italic, final String boldItalic ) {
		this.arg = null;
		this.regular = regular;
		this.bold = bold;
		this.italic = italic;
		this.boldItalic = boldItalic;
	}

	@Override
	public boolean equals( final Object other ) {
		if( other instanceof final JMo_IText_Font o2 )
			return this.regular.equals( o2.regular ) && this.bold.equals( o2.bold ) && this.italic.equals( o2.italic ) && this.boldItalic.equals( o2.boldItalic );
		return false;
	}

	@Override
	public boolean equalsLazy( final Object other ) {
		return this.equals( other );
	}

	public BaseFont getBaseFont( final CallRuntime cr, final boolean bold, final boolean italic ) {
		BaseFont result = null;

		try {
			final String fontname = this.iGetName( bold, italic );
			if( fontname == null )
				throw new RuntimeError( cr, "Invalid font", "An individual font name cannot be formatted to bold or italic." );
			result = BaseFont.createFont( fontname, BaseFont.CP1252, BaseFont.NOT_EMBEDDED );
		}
		catch( final DocumentException e ) {
			throw new ExternalError( cr, "PDF error", e.getMessage() );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "PDF font error", e.getMessage() );
		} // Kommt vmtl. seeehr selten vor!
		Err.ifNull( result );
		return result;
	}

	/**
	 * °HELVETICA()IText_Font # Constant for font "Helvetica"
	 * °COURIER()IText_Font # Constant for font "Courier"
	 * °TIMES()IText_Font # Constant for font "Times"
	 */
	@Override
	public A_Immutable getConstant( final CallRuntime cr, final String name ) {

		switch( name ) {
			case "HELVETICA":
				return new JMo_IText_Font( FontFactory.HELVETICA, FontFactory.HELVETICA_BOLD, FontFactory.HELVETICA_OBLIQUE, FontFactory.HELVETICA_BOLDOBLIQUE );
			case "COURIER":
				return new JMo_IText_Font( FontFactory.COURIER, FontFactory.COURIER_BOLD, FontFactory.COURIER_OBLIQUE, FontFactory.COURIER_BOLDOBLIQUE );
			case "TIMES":
				return new JMo_IText_Font( FontFactory.TIMES, FontFactory.TIMES_BOLD, FontFactory.TIMES_ITALIC, FontFactory.TIMES_BOLDITALIC );
			// TODO
		}

		throw new CodeError( cr, "Unknown constant", "Constant/Enum is not defined in <" + this.getTypeName() + ">: " + name );
	}

	@Override
	public void init( final CallRuntime cr ) {

		if( this.arg != null ) {
			final JMo_Str so = this.arg.init( cr, this, JMo_Str.class );
			this.regular = so.rawString();
		}
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {
		return null;
	}

	private String iGetName( final boolean bold, final boolean italic ) {
		if( !bold && !italic )
			return this.regular;
		if( bold && !italic )
			return this.bold;
		if( !bold && italic )
			return this.italic;
		return this.boldItalic;
	}

}
