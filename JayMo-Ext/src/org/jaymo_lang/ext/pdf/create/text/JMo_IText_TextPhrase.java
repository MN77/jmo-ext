/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.pdf.create.text;

import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Phrase;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class JMo_IText_TextPhrase extends A_ObjectSimple implements I_IText_TextItem {

	private String text = null;


	public JMo_IText_TextPhrase( final String text ) {
		Err.ifEmpty( text );
		this.text = text;
	}

	public void draw( final Document dokument ) {
		final Phrase erg = new Phrase( this.text );

//		if(this.hintergrund[0]!=255 || this.hintergrund[1]!=255 || this.hintergrund[2]!=255)
//			erg.setBackground(new Color(this.hintergrund[0],this.hintergrund[1],this.hintergrund[2]));
		try {
			dokument.add( erg );
		}
		catch( final DocumentException e ) {
			Err.newRuntime( "PDF-Error", e );
		}
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {
//		switch(method) {}
		return null;
	}

}
