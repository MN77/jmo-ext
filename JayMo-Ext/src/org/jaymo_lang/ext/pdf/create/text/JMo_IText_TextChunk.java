/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.pdf.create.text;

import java.awt.Color;

import org.jaymo_lang.lib.graphic.Util_Color;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class JMo_IText_TextChunk extends A_ObjectSimple implements I_IText_TextItem {

	private int[]  backgroundRGB;
	private String text = null;


	public JMo_IText_TextChunk( final String text ) {
		Err.ifEmpty( text );
		this.text = text;
		this.backgroundRGB = new int[]{ 255, 255, 255 };
	}

	public void draw( final Document dokument ) {
		final Chunk result = new Chunk( this.text );
//		if(this.hintergrund[0]!=255 || this.hintergrund[1]!=255 || this.hintergrund[2]!=255)
		result.setBackground( new Color( this.backgroundRGB[0], this.backgroundRGB[1], this.backgroundRGB[2] ) );

		try {
			dokument.add( result );
		}
		catch( final DocumentException e ) {
			Err.newRuntime( "PDF-Error", e );
		}

//		float subscript = -8.0f;
//		pdf().setTextRise(subscript);
//		pdf().setUnderline(new Color(0xFF, 0x00, 0x00), 3.0f, 0.0f, -5.0f + subscript, 0.0f, PdfContentByte.LINE_CAP_ROUND);
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "setBackground": // TODO: setColorBackground | setBackgroundColor
				return this.mSetBackground( cr );
		}
		return null;
	}

	/**
	 * °setBackground(Color color)Same # Set the background color of this text chunk
	 * °setBackground(Int r, Int g, Int b)Same # Set the background color of this text chunk
	 */
	private JMo_IText_TextChunk mSetBackground( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 3 );
		this.backgroundRGB = Util_Color.argsToRGB( cr, args );
		return this;
	}

}
