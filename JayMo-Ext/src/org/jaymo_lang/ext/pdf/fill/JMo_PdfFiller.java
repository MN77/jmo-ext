/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.pdf.fill;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Map;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfWriter;

import de.mn77.base.data.struct.I_Iterable;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.ext.pdf.fill.PDF_FillInfo;
import de.mn77.ext.pdf.fill.PDF_Filler;


/**
 * @author Michael Nitsche
 * @created 21.06.2018
 */
public class JMo_PdfFiller extends A_ObjectSimple {

	private final ArgCallBuffer par_source;
	private PDF_Filler          pdf;


	/**
	 * !Fill out a PDF form
	 * +PdfFiller(Str source)
	 */
	public JMo_PdfFiller( final Call source ) {
		this.par_source = new ArgCallBuffer( 0, source );
	}

	@Override
	public void init( final CallRuntime cr ) {
		final JMo_Str o = this.par_source.init( cr, this, JMo_Str.class );
		final String s = o.rawString();
		this.pdf = new PDF_Filler( s );
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "available":
			case "fields":
				return this.mFields( cr );
			case "fill":
				this.fill( cr );
				return this;

			default:
				return null;
		}
	}

//	-------------------------------------------------------------------

	/**
	 * °fill(Str target, List page_maps, Map pdfinfo)Same # Fills out the source-pdf and write it to target.
	 */
	private void fill( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Str.class, JMo_List.class, JMo_Map.class );
		final JMo_Str target = (JMo_Str)args[0];
		final JMo_List pages = (JMo_List)args[1];
		final JMo_Map info = (JMo_Map)args[2];

		final I_List<Map<String, String>> pdfPages = new SimpleList<>();

		for( final I_Object page : pages.getInternalCollection() ) {
			final I_Table<I_Object> pageMap = ((JMo_Map)page).copyToTable();
			final HashMap<String, String> daten = new HashMap<>();

			for( final I_Object[] pm : pageMap )
				daten.put( Lib_Convert.toStr( cr, pm[0] ).rawString(), Lib_Convert.toStr( cr, pm[1] ).rawString() );

//			daten.describe();
//			pdf.fill("/tmp/ziel1.pdf", daten);
			pdfPages.add( daten );
		}

		final PDF_FillInfo pdfinfo = new PDF_FillInfo();

		for( final I_Object[] iof : info.copyToTable() ) {
			final String s1 = Lib_Convert.toStr( cr, iof[0] ).rawString().toLowerCase();
			final String s2 = Lib_Convert.toStr( cr, iof[1] ).rawString();

			switch( s1 ) {
				case "author":
					pdfinfo.AUTHOR.set( s2 );
					break;
				case "creator":
					pdfinfo.ERSTELLER.set( s2 );
					break;
				case "title":
					pdfinfo.TITEL.set( s2 );
					break;
				case "subject":
					pdfinfo.BETREFF.set( s2 );
					break;
				case "compress":
					pdfinfo.COMPRESS.set( s2.equals( "true" ) );
					break;
				case "version":
					switch( s2 ) {
						case "1.2":
							pdfinfo.PDF_VERSION.set( PdfWriter.PDF_VERSION_1_2 );
							break;
						case "1.3":
							pdfinfo.PDF_VERSION.set( PdfWriter.PDF_VERSION_1_3 );
							break;
						case "1.4":
							pdfinfo.PDF_VERSION.set( PdfWriter.PDF_VERSION_1_4 );
							break;
						case "1.5":
							pdfinfo.PDF_VERSION.set( PdfWriter.PDF_VERSION_1_5 );
							break;
						case "1.6":
							pdfinfo.PDF_VERSION.set( PdfWriter.PDF_VERSION_1_6 );
							break;
						case "1.7":
							pdfinfo.PDF_VERSION.set( PdfWriter.PDF_VERSION_1_7 );
							break;

						default:
							throw new RuntimeError( cr, "Invalid value for PDF-version", "Got: " + s2 );
					}
					break;
				case "conformance":
					switch( s2 ) {
						case "PDFA1A":
							pdfinfo.PDF_FORMAT.set( PdfWriter.PDFA1A );
							break;
						case "PDFA1B":
							pdfinfo.PDF_FORMAT.set( PdfWriter.PDFA1B );
							break;

						default:
							throw new RuntimeError( cr, "Invalid conformance value", "Got: " + s2 );
					}
					break;

				default:
					throw new RuntimeError( cr, "Unknown Document-Info-Fieldname", "Got field: " + s1 );
			}
		}

		final String target2 = target.rawString();

		try {
			this.pdf.fill( new FileOutputStream( target2 ), target2 + ".temp", pdfPages, pdfinfo );
		}
		catch( Err_FileSys | FileNotFoundException | DocumentException e ) {
			Err.exit( e );
		}
	}

	/**
	 * °available ^ fields
	 * °fields()List # Get a list with all possible field-names
	 */
	private JMo_List mFields( final CallRuntime cr ) {
		cr.argsNone();

		try {
			final I_Iterable<String> fields = this.pdf.getFieldNames();
			final SimpleList<I_Object> al = new SimpleList<>( fields.size() );
			for( final String field : fields )
				al.add( new JMo_Str( field ) );
			return new JMo_List( al );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "File-Error", e.getMessage() );
		}
	}

}
