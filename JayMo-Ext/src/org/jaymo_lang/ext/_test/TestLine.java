/*******************************************************************************
 * Copyright (C) 2017-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext._test;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.model.App;
import org.jaymo_lang.parser.Parser_App;

import de.mn77.base.debug.DEBUG_MODE;
import de.mn77.base.debug.Stopwatch;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class TestLine {

//	String s = "Json.parse(\"{\\\"first\\\": 123, \\\"second\\\": [4, 5, 6], \\\"third\\\": 789}\").print.get.print";
	String s = "db = HSqlDB.open; db.update(\"CREATE TABLE foo ( id INTEGER IDENTITY NOT NULL PRIMARY KEY, test INTEGER NOT NULL);\").print; db.query(\"SELECT * FROM foo\").print; db.update(\"INSERT INTO foo(test) VALUES (9)\").print; db.query(\"SELECT * FROM foo\").table.print";
//	String s = "m=SWT_Main;m.add(SWT_ComboBox(false).addAll([\"a\",\"b\",\"c\"]));m.run";

//	String search = "F.";
//	String s = "?"+this.search+";Print '#'*40;??"+this.search;

//	private static void start() throws Err_FileSys {
//		MOut.setDebug(DEBUG_LEVEL.NO);

	// Läuft
//		run("Test.hello.print");
//		run("a=AutoTest; a.getTrue.print; a.getFalse.print; a.test.print; a.test(false).print");
//		run("a=LibAutoTest; a.getTrue.print; a.getFalse.print; a.test.print; a.test(false).print");


//		run("Speak.say(\"Hallo\")");
//		run("s=Speak; s.setVoice(\"dfki-pavoque-neutral\"); s.say(\"Hallo\"); s.say(\"5\")");	//TODO Warten bis MOut fertig!
//		run("s=Speak; s.list.print; s.setVoice(\"dfki-pavoque-neutral\"); s.say(\"Hallo Foo! Wie gehts dir? Was machst du heute noch?\")");

//		TestLine.run("HSqlDB.open.exec(\"CREATE TABLE foo ( id INTEGER IDENTITY NOT NULL PRIMARY KEY, test INTEGER NOT NULL);\")");
//		TestLine.run("HSqlDB");
//		TestLine.run("MaryTTS");

//		TestLine.run("HSqlDB.open.exec(\"CREATE TABLE foo ( id INTEGER IDENTITY NOT NULL PRIMARY KEY, test DATE NOT NULL); INSERT INTO foo (id,test) VALUES (1,CURDATE); SELECT * FROM foo WHERE id=1;\").getTable");

	//	TestLine.run("db = HSqlDB(\"/tmp/foo/db\").open.exec(\"CREATE TABLE foo ( id INTEGER IDENTITY NOT NULL PRIMARY KEY ); INSERT INTO foo (id) VALUES (2); SELECT * FROM foo;\").getTable.print; db.close");
	//	TestLine.run("db = HSqlDB.open.exec(\"CREATE TABLE foo ( id INTEGER IDENTITY NOT NULL PRIMARY KEY ); INSERT INTO foo (id) VALUES (2); SELECT * FROM foo;\").getTable.print; db.close");


	//	TestLine.run("db = HSqlDB.open.exec(\"CREATE TABLE foo ( id INTEGER IDENTITY NOT NULL PRIMARY KEY )\"); db.exec(\"INSERT INTO foo (id) VALUES (2);\"); db.query(\"SELECT * FROM foo;\").getTable.print; db.close");
	//	TestLine.run("db = HSqlDB.open.exec(\"CREATE TABLE foo ( id INTEGER IDENTITY NOT NULL PRIMARY KEY );\"); db.query(\"SELECT * FROM foo;\").getTable.print; db.close");


//		TestLine.run("db = HSqlDB(\"/home/mike/Prog/JMo/bricklink/db/bricklink\").open; db.query(\"SELECT * FROM colors;\").getTable.print; db.close");


//	}

//--------------------------------------------------------------------------------------------------------------------


	public static void main( final String[] args ) {

		try {
			final Stopwatch uhr = new Stopwatch();
			final TestLine tl = new TestLine();
			tl.run( tl.s, args );
//			TestLine.start();
			MOut.print( "-----------------------------------" );
			MOut.setDebug( DEBUG_MODE.MINIMAL );
			uhr.print();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	private void run( final String s, final String... args ) throws Err_FileSys {
		final Parser_App parser = new Parser_App();
		final App app = parser.parseText( s );

		MOut.print( JayMo.NAME + "  " + parser.getVersionString( false, true, true ) );

		MOut.print( "-----------------------------------" );
		app.describe();
		MOut.print( "-----------------------------------" );
		app.exec( args );
	}

}
