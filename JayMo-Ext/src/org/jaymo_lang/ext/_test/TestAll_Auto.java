/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext._test;

import org.jaymo_lang._test.util.AutoTest;
import org.jaymo_lang._test.util.FAIL_ACTION;

import de.mn77.base.debug.DEBUG_MODE;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 09.10.2020
 */
public class TestAll_Auto {

	private static final FAIL_ACTION AT_FAIL    = FAIL_ACTION.STOP;
	private static final DEBUG_MODE  DEBUG      = DEBUG_MODE.MINIMAL;
	private static final boolean     PARSE_ONLY = false;


	public static void main( final String[] args ) {
		MOut.setDebug( TestAll_Auto.DEBUG );

		try {
			final String testdir = "/home/mike/Prog/JayMo/ext/tests_auto";

			final AutoTest tester = new AutoTest( TestAll_Auto.AT_FAIL, TestAll_Auto.DEBUG, TestAll_Auto.PARSE_ONLY );
			tester.testDir( testdir );
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
