/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.dialog;

import org.eclipse.swt.widgets.Display;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.error.I_ErrorDetails;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 *
 *         TODO Hier muß noch das Log berücksichtigt werden!
 *         TODO Sollte evtl. in einen SyncThread gepackt werden, somit auch jederzeit von externen Threads aufrufbar
 *         TODO Evtl. die Trennung zum beenden durch Fehler/Hinweis realisieren
 *         TODO Wenn Fehler auftreten, wo noch keine Shell angezeigt wird, wird auch kein Dialog geöffnet!
 *         TODO Warum null zurückgeben? Dann doch besser void, oder?
 */
public class SWT_Error {

	public static RuntimeException create( final CallRuntime cr, final String fehlertext, final boolean ende ) {

		try {
			throw Err.newRuntime( fehlertext );
		}
		catch( final Err_Runtime f ) {
			return SWT_Error.show( cr, f, null, ende );
		}
	}

	public static RuntimeException exit( final CallRuntime cr, final Throwable t ) {
		return SWT_Error.show( cr, t, null, true );
	}

	public static RuntimeException exit( final CallRuntime cr, final Throwable t, final String text ) {
		return SWT_Error.show( cr, t, text, true );
	}

	public static RuntimeException show( final CallRuntime cr, final Throwable t ) {
		return SWT_Error.show( cr, t, null, false );
	}

	public static RuntimeException show( final CallRuntime cr, final Throwable t, final String text ) {
		return SWT_Error.show( cr, t, text, false );
	}

	private static RuntimeException show( final CallRuntime cr, final Throwable t, final String text, final boolean ende ) {
		// Zuerst immer auf der Console ausgeben!
		MOut.error( t, text );

		// Text aufbereiten:
		Err.ifNull( t );
		final StringBuilder sb = new StringBuilder();

		if( text != null )
			sb.append( text + "\n\n" );

		Throwable basis = t;

		do {
			if( basis != null )
				sb.append( basis.getMessage() + "\n\n" );

			if( basis instanceof final I_ErrorDetails bf )
				for( final Object o : bf.getDetails() )
					sb.append( ConvertObject.toStringIdent( o ) + "\n\n" );

			if( basis != null )
				basis = basis.getCause();
		}
		while( basis != null );

		if( ende )
			sb.append( "The application will be terminated!" );

		// Dialog anzeigen
		final Display d = Display.getCurrent();

		if( d != null ) {
			final Runnable r = () -> {
				final SWT_Dialog meldung = new SWT_Dialog( sb.toString() ).setTypeError();
				meldung.open( cr, ende ); //Bei Absturz muß gewartet werden, damit der Dialog auch angezeigt wird!!!
			};
			d.asyncExec( r );
		}
//		else { // Wenn kein Display verfügbar, nichts tun! Wurde ja bereits ausgegeben!
		// Kein Display (mehr) verfügbar
//			Err.show(t, true, "No Display for Dialog! -> "+text);		// TODO Prüfen! hier direkt beenden?
//		}
		if( ende )
			System.exit( 1 );
		return null;
	}

}
