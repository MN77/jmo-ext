/*******************************************************************************
 * Copyright (C) 2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.core;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.jaymo_lang.ext.swt.layout.LayoutMax;
import org.jaymo_lang.lib.gui.control.scroll.composit.canvas.deco.A_JG_WindowM;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 28.01.2025
 */
public abstract  class A_SWT_Window extends A_SWT_Object<Composite> {

	private int                               height = 200;
	private I_SWT_Object<? extends Composite> layout = null;
	private String                            title  = null;
	private int                               width  = 200;
//	private I_SWT_Window window;

	private final A_JG_WindowM methods = new A_JG_WindowM() {

		@Override
		protected void add( final I_Object obj ) {
			final I_SWT_Object<?> swtObj = (I_SWT_Object<?>)obj; // TODO Type ...  now okay?
			A_SWT_Window.this.layout = new LayoutMax( swtObj );
		}

		@Override
		protected void close( final CallRuntime cr ) {
			A_SWT_Window.this.getDisplay().close();
		}

		@Override
		protected String getTitle() {
			return A_SWT_Window.this.title;
		}

		@Override
		protected void setSize( final int width, final int height ) {
			A_SWT_Window.this.width = width;
			A_SWT_Window.this.height = height;
		}

		@Override
		protected void setTitle( String title ) {
//			this.gHauptfenster().sTitel(sTitle);	// TODO Automatische Prüfung ob schon erzeugt!
			A_SWT_Window.this.title = title;
		}
	};

	@Override
	public void init( final CallRuntime cr ) {}

	@Override
	public Composite swt() {
		// TODO Wirklich null ?!?
		return this.layout == null ? null : this.layout.swt();
	}

	@Override
	public boolean validateEvent( final String event ) {
		return event.equals( "@ready" );
	}

	/**
	 * °add(SWT_Object obj)Same # Add component
	 * °getTitle()Str # Return the window title
	 * °setSize(IntNumber width, IntNumber height)Same # Set window size
	 * °setTitle(Str title)Same # Set window title
	 * °run()Same # Start and open main window
	 * °close()Same # Close main window
	 */
	@Override
	protected final I_Object callMethod( final CallRuntime cr, final String method ) {
		//TODO Für alle Funktionen einen automatischen Puffer, der beim erstellen abgearbeitet wird und danach direkt funktioniert!

		final I_Object result = this.methods.call( cr, method, this );

		return result == null
			? this.callMethod2(cr, method)
			: result;
	}

	protected abstract I_Object callMethod2( final CallRuntime cr, final String method );


	protected Display getDisplay() {
		return this.swt() == null || this.swt().isDisposed() ? null : this.swt().getDisplay(); // return Display.getCurrent() ?
	}

	protected I_SWT_Frame getWindow() {
		return (I_SWT_Frame)this.swt().getShell().getData();
	}

	@Override
	protected boolean isCreated() {
		return this.layout != null;
	}

	// TODO evtl. z.B. auch einen SWT_Knopf erlauben
	protected I_SWT_Object<? extends Composite> layout() {
		return this.layout;
	}

}
