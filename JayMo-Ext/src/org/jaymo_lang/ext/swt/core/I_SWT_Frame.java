/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.core;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.event.Procedure;


/**
 * @author Michael Nitsche
 * @created 12.02.2010
 */
public interface I_SWT_Frame extends I_SWT_Object<Shell> {

	void close();


	void createSWT( CallRuntime cr, Display display, Shell parent );

	void open( CallRuntime cr );

	void open( CallRuntime cr, Shell parent );

	void open( CallRuntime cr, Shell parent, Procedure bereit );


	void startAsync( CallRuntime cr, Runnable r );

	Thread startOutside( CallRuntime cr, Runnable r );

	void startSync( CallRuntime cr, Runnable r );

}
