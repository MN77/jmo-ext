/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.graphic;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Display;
import org.jaymo_lang.ext.swt.utils.Lib_ImageSWT;
import org.jaymo_lang.lib.graphic.JMo_Image;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.I_File;
import de.mn77.lib.graphic.MImage;
import de.mn77.lib.graphic.MImageX;


/**
 * @author Michael Nitsche
 */
public class SWT_Image { // implements I_ImageBase<Image>

	private Image image = null;


	public static SWT_Image newDirect( final Object image ) { // throws Err_FileSys
		Err.ifNull( image );

		return image instanceof SWT_Image
			? (SWT_Image)image
			: new SWT_Image( image );
	}

	public SWT_Image( final I_File img ) throws Err_FileSys {
		Err.fsIfMissing( img );
		this.image = this.iLoad( img );
	}

	public SWT_Image( Object img ) {
		Err.ifNull( img );

		if( img instanceof String[] && ((String[])img).length > 0 )
			img = ((String[])img)[0];

		if( img instanceof Image )
			/* MOut.warnung("Image wird direkt verwendet und ggf. von E_SWT_Bild freigegeben!"); TODO */
			this.image = (Image)img;
//		else if(img instanceof I_ImageBase)
		else if( img instanceof MImage )
			this.image = new Image( Display.getDefault(), Lib_ImageSWT.convert_AWT_to_SWT( ((MImage)img).getImage() ) );
		else if( img instanceof ImageData )
			this.image = new Image( Display.getDefault(), (ImageData)img );
		else if( img instanceof InputStream )
			this.image = this.iLoad( img );
		else if( img instanceof SWT_Image )
			this.image = new Image( null, ((SWT_Image)img).getImage(), SWT.IMAGE_COPY );
		else if( img instanceof JMo_Image )
			this.image = new Image( Display.getDefault(), Lib_ImageSWT.convert_AWT_to_SWT( ((JMo_Image)img).internalGet().getImage() ) );
		else if( img instanceof Integer )
			this.image = Display.getDefault().getSystemImage( (Integer)img );
		else if( img instanceof String ) {
			if( new File( "" + img ).exists() )
				this.image = this.iLoad( img );
			else
				throw Err.invalid( "File is missing", img );
		}
		else
			throw Err.invalid( "Invalid class", img.getClass().getCanonicalName() );
	}

	public SWT_Image copy() {
		return new SWT_Image( new Image( null, this.image, SWT.IMAGE_COPY ) ); //beachte auch Konstruktor
	}

//	public void disabledImage() {
//		Image temp = new Image(null, this.image, SWT.IMAGE_GRAY);
////		Image temp = new Image(null, this.bild, SWT.IMAGE_DISABLE);
//		final ImageData data = temp.getImageData();
//		final int width = data.width;
//		final int height = data.height;
//		for(int y = 0; y < height; y++)
//			for(int x = 0; x < width; x++)
//				data.setAlpha(x, y, data.getAlpha(x, y) / 3); //Durch 4 ist zu hell und /2 ist zu dunkel. Und fix (-128) ist nicht schön
//		//Die 3 Zeilen sind für Linux nicht nötig, aber funktioniert sonst in Win nicht:
//		temp = new Image(null, data);
//		this.image.dispose();
//		this.image = temp;
//	}

	public void dispose() {
		this.image.dispose();
	}

	public int getHeight() {
		return this.image.getBounds().height;
	}

	public Image getImage() {
		return this.image;
	}

	public int getWidth() {
		return this.image.getBounds().width;
	}

	public boolean isDisposed() {
		return this.image.isDisposed();
	}

	public JMo_Image toJayMoImage() {
		final ImageData data = this.image.getImageData();
		final BufferedImage bi = Lib_ImageSWT.convert_SWT_to_AWT( data );
		final MImageX img = new MImageX( bi );
		return new JMo_Image( img );
	}

	private Image iLoad( final Object o ) {
		//Viel langsamer als Image(null,o) das native Methoden nutzt, macht aber unter Win Probleme bei Transparenz!
		final ImageLoader loader = new ImageLoader();
		final ImageData[] data = o instanceof InputStream ? loader.load( (InputStream)o ) : loader.load( "" + o );
		return new Image( null, data[0] );
	}

}
