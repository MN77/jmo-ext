/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.layout;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Control;
import org.jaymo_lang.ext.swt.widget.A_SWT_Component;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 *         10.03.2009 Erstellt
 *
 *         Gitter zieht automatisch auf jeder Seite um 5px ein, hier nicht.
 *
 *         TODO Zwischenräume sollten auch automatisch auf 0 gesetzt werden
 *         TODO erweitern um marginLeft, marginRight = Einzug
 */
public class JMo_SWT_LayoutGrid extends A_SWT_Layout {

	public static final String AUSGLEICHEN    = "_A";
	public static final String AUSSEN_H       = "_AH";
	public static final String AUSSEN_V       = "_AV";
	public static final String COMPONENT      = "_K";
	public static final String SPALTEN        = "_SA";
	public static final String ZWISCHENRAUM_H = "_ZH";
	public static final String ZWISCHENRAUM_V = "_ZV";


	private final ArgCallBuffer                            cellsH;
	private final ArgCallBuffer                            cellsV;
	private final HashMap<String, Object>                  gitter;
	private ArrayList<HashMap<String, A_SWT_Component<?>>> buffer;


//	public JMo_SWT_LayoutGrid() {
//	}


	public JMo_SWT_LayoutGrid( final Call width, final Call height ) {
//		this();
		this.buffer = new ArrayList<>();
		this.gitter = new HashMap<>();
		this.cellsH = new ArgCallBuffer( 0, width );
		this.cellsV = new ArgCallBuffer( 1, height );
	}

	/**
	 * °add(SWT_Component comp...)Same # Add one or more components.
	 */
	public JMo_SWT_LayoutGrid mAdd( final CallRuntime cr ) {
		final A_SWT_Component<?>[] args = (A_SWT_Component<?>[])cr.argsVar( this, 1, A_SWT_Component.class );

//		this.pending( () -> {
		for( final A_SWT_Component<?> o : args ) {
			final HashMap<String, A_SWT_Component<?>> sl = new HashMap<>();
			sl.put( JMo_SWT_LayoutGrid.COMPONENT, o );
			this.buffer.add( sl );
//				this.swt().add
		}
//		});


//		I_SWT_Component<?> komp= ka.length==0
//			? new JMo_LayoutSpace()
//			: ka[0];
//
//		Err.ifNull(komp);
//		I_KeyList<String,Object> sl=new KeyList<>();
//		sl.add(JMo_SWT_LayoutGrid.KOMPONENTE, komp);
////		if(!this.istErzeugt())
////			this.puffer.add(sl);
////		else
////			Err.invalid("Layout bereits erzeugt!");
//		return new H_LayoutGitter(this, sl);
		return this;
	}

	public JMo_SWT_LayoutGrid sAussenabstand( final int loru ) {
		Err.ifTooSmall( 0, loru );
		this.sAussenabstand( loru, loru );
		return this;
	}

	public JMo_SWT_LayoutGrid sAussenabstand( final int linksRechts, final int obenUnten ) {
		Err.ifTooSmall( 0, linksRechts );
		Err.ifTooSmall( 0, obenUnten );
		if( linksRechts == 0 && obenUnten == 0 )
			MOut.dev( "0 muss nicht mehr extra gesetzt werden!" );
		this.gitter.put( JMo_SWT_LayoutGrid.AUSSEN_H, linksRechts );
		this.gitter.put( JMo_SWT_LayoutGrid.AUSSEN_V, obenUnten );
		return this;
	}

	public JMo_SWT_LayoutGrid sSpaltenAusgleichen() {
		this.gitter.put( JMo_SWT_LayoutGrid.AUSGLEICHEN, true );
		return this;
	}

//	public JMo_SWT_LayoutGrid sSpaltenAnzahl(int spalten) {
//		Err.ifToSmall(1, spalten);
//		this.gitter.add(JMo_SWT_LayoutGrid.SPALTEN, spalten);
//		return this;
//	}

	public JMo_SWT_LayoutGrid sZwischenraum( final int breiteHoehe ) {
		this.sZwischenraumH( breiteHoehe );
		this.sZwischenraumV( breiteHoehe );
		return this;
	}

	public JMo_SWT_LayoutGrid sZwischenraumH( final int breite ) {
		Err.ifTooSmall( 0, breite );
		if( breite == 0 )
			MOut.dev( "0 muss nicht mehr extra gesetzt werden!" );
		this.gitter.put( JMo_SWT_LayoutGrid.ZWISCHENRAUM_H, breite );
		return this;
	}

	public JMo_SWT_LayoutGrid sZwischenraumV( final int hoehe ) {
		Err.ifTooSmall( 0, hoehe );
		if( hoehe == 0 )
			MOut.dev( "0 muss nicht mehr extra gesetzt werden!" );
		this.gitter.put( JMo_SWT_LayoutGrid.ZWISCHENRAUM_V, hoehe );
		return this;
	}

	protected void iAddComponent( final CallRuntime cr, final HashMap<String, A_SWT_Component<?>> data ) {
		final A_SWT_Component<?> k = data.get( JMo_SWT_LayoutGrid.COMPONENT );
		k.createSWT( cr, this.swt() );
		final GridData dat = this.iGetLayoutData( k );


		// Default
		dat.grabExcessHorizontalSpace = true;
		dat.grabExcessVerticalSpace = true;
		dat.horizontalAlignment = SWT.FILL;
		dat.verticalAlignment = SWT.FILL;

		//TODO Abweichend:


//		if(data.getKeys().contains(H_LayoutGitter.HOEHE))
//			dat.heightHint=(Integer)data.get(H_LayoutGitter.HOEHE);
//		else if(data.getKeys().contains(H_LayoutGitter.FLEXIBEL_V))
//			dat.grabExcessVerticalSpace=true;
//
//		if(data.getKeys().contains(H_LayoutGitter.BREITE))
//			dat.widthHint=(Integer)data.get(H_LayoutGitter.BREITE);
//		else if(data.getKeys().contains(H_LayoutGitter.FLEXIBEL_H))
//			dat.grabExcessHorizontalSpace=true;
//
//		if(data.getKeys().contains(H_LayoutGitter.POSITION_V))
//			dat.verticalAlignment = Lib_Position.selectV((POSITION_V)data.get(H_LayoutGitter.POSITION_V), SWT.BEGINNING, SWT.CENTER, SWT.END);
//		else
//			dat.verticalAlignment=SWT.FILL;
//
//		if(data.getKeys().contains(H_LayoutGitter.POSITION_H))
//			dat.horizontalAlignment = Lib_Position.selectH((POSITION_H)data.get(H_LayoutGitter.POSITION_H), SWT.BEGINNING, SWT.CENTER, SWT.END);
//		else
//			dat.horizontalAlignment=SWT.FILL;

		//Es gibt ja teils Default-Höhen/Breiten
//		if(!data.gSchluessel().kennt(H_LayoutGitter.HOEHE) && data.gSchluessel().kennt(H_LayoutGitter.POSITION_V))
//			Err.forbidden("Vertikale Position ohne Höhe zu setzen bringt nichts!");
//		if(!data.gSchluessel().kennt(H_LayoutGitter.BREITE) && data.gSchluessel().kennt(H_LayoutGitter.POSITION_H))
//			Err.forbidden("Horizontale Position ohne Breite zu setzen bringt nichts!");

//		if(data.getKeys().contains(H_LayoutGitter.HOEHE) && data.getKeys().contains(H_LayoutGitter.FLEXIBEL_V))
//			Err.forbidden("Flexible und feste Höhe sind gesetzt! Was denn nun???");
//		if(data.getKeys().contains(H_LayoutGitter.BREITE) && data.getKeys().contains(H_LayoutGitter.FLEXIBEL_H))
//			Err.forbidden("Flexible und feste Breite sind gesetzt! Was denn nun???");
//		//Das beißt sich in SWT, deshalb raus:
//		if(data.getKeys().contains(H_LayoutGitter.POSITION_V) && data.getKeys().contains(H_LayoutGitter.FLEXIBEL_V))
//			Err.forbidden("Position und Flexibel wird mit SWT ohne Flexibel dargestellt!");
//		if(data.getKeys().contains(H_LayoutGitter.POSITION_H) && data.getKeys().contains(H_LayoutGitter.FLEXIBEL_H))
//			Err.forbidden("Position und Flexibel wird mit SWT ohne Flexibel dargestellt!");
//
//		if(data.getKeys().contains(H_LayoutGitter.UEBERGREIFEND_H))
//			dat.horizontalSpan=(Integer)data.get(H_LayoutGitter.UEBERGREIFEND_H);
//		if(data.getKeys().contains(H_LayoutGitter.UEBERGREIFEND_V))
//			dat.verticalSpan=(Integer)data.get(H_LayoutGitter.UEBERGREIFEND_V);
//
//		if(data.getKeys().contains(H_LayoutGitter.EINRUECKEN_H))
//			dat.horizontalIndent=(Integer)data.get(H_LayoutGitter.EINRUECKEN_H);
//		if(data.getKeys().contains(H_LayoutGitter.EINRUECKEN_V))
//			dat.verticalIndent=(Integer)data.get(H_LayoutGitter.EINRUECKEN_V);

//		swt().layout();
	}

	@Override
	protected void jmoInit4( final CallRuntime cr ) {
		final I_Object objH = this.cellsH.init( cr, this, I_IntNumber.class );
//		final I_Object objV = this.cellsV.init(cr, this, I_Integer.class); //TODO

		final int columns = Lib_Convert.toInt( cr, objH );
		this.gitter.put( JMo_SWT_LayoutGrid.SPALTEN, columns );
//		this.gitter.add(JMo_SWT_LayoutGrid.SPALTEN, spalten);
	}

	@Override
	protected I_Object jmoMethod4( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "add":
				return this.mAdd( cr );
		}
		return null;
	}

	@Override
	protected void swtInit4( final CallRuntime cr ) {
		final GridLayout layout = new GridLayout();
		this.swt().setLayout( layout );

		this.iSetzeLayout( layout, this.gitter );

		for( final HashMap<String, A_SWT_Component<?>> sl : this.buffer )
			this.iAddComponent( cr, sl );
		this.buffer = null;
		this.swt().layout();
	}

	private GridData iGetLayoutData( final A_SWT_Component<?> component ) {
		GridData result = (GridData)this.swt().getLayoutData();

		if( result == null ) {
			result = new GridData();
			((Control)component.swt()).setLayoutData( result );
		}
		return result;
	}

	private void iSetzeLayout( final GridLayout layout, final HashMap<String, Object> gitter ) {
		if( gitter.keySet().contains( JMo_SWT_LayoutGrid.SPALTEN ) )
			layout.numColumns = (Integer)gitter.get( JMo_SWT_LayoutGrid.SPALTEN );

		if( gitter.keySet().contains( JMo_SWT_LayoutGrid.ZWISCHENRAUM_H ) )
			layout.horizontalSpacing = (Integer)gitter.get( JMo_SWT_LayoutGrid.ZWISCHENRAUM_H );
		else
			layout.horizontalSpacing = 0;
		if( gitter.keySet().contains( JMo_SWT_LayoutGrid.ZWISCHENRAUM_V ) )
			layout.verticalSpacing = (Integer)gitter.get( JMo_SWT_LayoutGrid.ZWISCHENRAUM_V );
		else
			layout.verticalSpacing = 0;

		if( gitter.keySet().contains( JMo_SWT_LayoutGrid.AUSGLEICHEN ) )
			layout.makeColumnsEqualWidth = true;

		if( gitter.keySet().contains( JMo_SWT_LayoutGrid.AUSSEN_H ) )
			layout.marginWidth = (Integer)gitter.get( JMo_SWT_LayoutGrid.AUSSEN_H );
		else
			layout.marginWidth = 0;
		if( gitter.keySet().contains( JMo_SWT_LayoutGrid.AUSSEN_V ) )
			layout.marginHeight = (Integer)gitter.get( JMo_SWT_LayoutGrid.AUSSEN_V );
		else
			layout.marginHeight = 0;
	}

}
