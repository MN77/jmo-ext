/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.layout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.jaymo_lang.ext.swt.widget.A_SWT_Component;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 */
public class JMo_SWT_LayoutBorder extends A_SWT_Layout {

	private A_SWT_Component<?> controlBottom = null;
	private A_SWT_Component<?> controlCenter = null;
	private A_SWT_Component<?> controlLeft   = null;
	private A_SWT_Component<?> controlRight  = null;
	private A_SWT_Component<?> controlTop    = null;


	public JMo_SWT_LayoutBorder() {}

	@Override
	protected void jmoInit4( final CallRuntime cr ) {}

	@Override
	protected I_Object jmoMethod4( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "put":
				return this.mPut( cr );
		}
		return null;
	}


	// ERZEUGEN

	@Override
	protected void swtInit4( final CallRuntime cr ) {
//		FillLayout rootLayout = new FillLayout();
//		rootLayout.type = SWT.VERTICAL;
//		this.swt().setLayout(rootLayout);

		int width = 0;
		if( this.controlLeft != null )
			width++;
		if( this.controlCenter != null )
			width++;
		if( this.controlRight != null )
			width++;
		if( width == 0 )
			width = 1;

//		MOut.temp(width);


//		Composite grid = new Composite(this.swt(), 0);
		final Composite grid = this.swt();
		final GridLayout gridLayout = new GridLayout( width, false );
		grid.setLayout( gridLayout );

		if( this.controlTop != null ) {
			final GridData data = new GridData();
			data.horizontalSpan = width;
			data.verticalAlignment = SWT.FILL;
			data.horizontalAlignment = SWT.FILL;
			data.grabExcessVerticalSpace = true;
			data.grabExcessHorizontalSpace = true;
			final Control top = (Control)this.controlTop.createSWT( cr, grid );
			top.setLayoutData( data );
		}

		if( this.controlLeft != null || this.controlCenter != null || this.controlRight != null ) {

			if( this.controlLeft != null ) {
				this.controlLeft.createSWT( cr, grid );
				final GridData data = new GridData();
				data.verticalAlignment = SWT.FILL;
				data.horizontalAlignment = SWT.FILL;
				data.grabExcessVerticalSpace = true;
//				data.grabExcessHorizontalSpace=true;
				((Control)this.controlLeft.swt()).setLayoutData( data );
			}

			if( this.controlCenter != null ) {
				this.controlCenter.createSWT( cr, grid );
				final GridData data = new GridData();
				data.verticalAlignment = SWT.FILL;
				data.horizontalAlignment = SWT.FILL;
				data.grabExcessVerticalSpace = true;
				data.grabExcessHorizontalSpace = true;
				((Control)this.controlCenter.swt()).setLayoutData( data );
			}

			if( this.controlRight != null ) {
//				FillLayout flowLayout = new FillLayout();
//				Composite cell = new Composite(grid, 0);
//				cell.setLayout(flowLayout);

				this.controlRight.createSWT( cr, grid );
//				this.controlRight.createSWT(cr, cell);
				final GridData data = new GridData();
				data.verticalAlignment = SWT.FILL;
				data.horizontalAlignment = SWT.FILL;
				data.grabExcessVerticalSpace = true;
//				data.grabExcessHorizontalSpace=true;
				((Control)this.controlRight.swt()).setLayoutData( data );
			}
		}

		if( this.controlBottom != null ) {
			final GridData data = new GridData();
			data.horizontalSpan = width;
			data.verticalAlignment = SWT.FILL;
			data.horizontalAlignment = SWT.FILL;
			data.grabExcessVerticalSpace = true;
			data.grabExcessHorizontalSpace = true;
			final Control top = (Control)this.controlBottom.createSWT( cr, grid );
			top.setLayoutData( data );
		}

		this.swt().layout();
	}

	/**
	 * °put(MagicPosition pos, SWT_Component comp)Same # Add component to this layout.
	 */
	private JMo_SWT_LayoutBorder mPut( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, MagicPosition.class, A_SWT_Component.class );
		this.checkNotCreated();
		final POSITION pos = ((MagicPosition)args[0]).get();
		final A_SWT_Component<?> obj = (A_SWT_Component<?>)args[1];

		if( pos == POSITION.TOP )
			this.controlTop = obj;
		else if( pos == POSITION.LEFT )
			this.controlLeft = obj;
		else if( pos == POSITION.CENTER )
			this.controlCenter = obj;
		else if( pos == POSITION.RIGHT )
			this.controlRight = obj;
		else if( pos == POSITION.BOTTOM )
			this.controlBottom = obj;
		else
			throw Err.invalid( pos );

		return this;
	}

}
