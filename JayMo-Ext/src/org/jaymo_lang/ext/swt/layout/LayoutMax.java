/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.layout;

import org.eclipse.swt.layout.FillLayout;
import org.jaymo_lang.ext.swt.core.I_SWT_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @apiNote Has only one maximized component.
 */
public class LayoutMax extends A_SWT_Layout {

	private I_SWT_Object<?> content;


	public LayoutMax( final I_SWT_Object<?> component ) {
		this.set( component );
	}

	public LayoutMax set( final I_SWT_Object<?> component ) {
		this.checkNotCreated();
		this.content = component;
		return this;
	}

//	public LayoutMax replace( I_SWT_Component<?> component ) {
//		if(this.component !=null)
//			((Composite)this.component.swt()).dispose();
//		this.component=component;
//		this.component.createSWT(cr, this.swt());
//		return this;
//	}

	@Override
	protected void jmoInit4( final CallRuntime cr ) {}

	@Override
	protected I_Object jmoMethod4( final CallRuntime cr, final String method ) {
		return null;
	}

	@Override
	protected void swtInit4( final CallRuntime cr ) {
		this.swt().setLayout( new FillLayout() );
		if( this.content != null )
			this.content.createSWT( cr, this.swt() );
	}

}
