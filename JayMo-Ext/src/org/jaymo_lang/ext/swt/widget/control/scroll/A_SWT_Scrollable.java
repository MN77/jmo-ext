/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget.control.scroll;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Scrollable;
import org.jaymo_lang.ext.swt.widget.control.A_SWT_Control;
import org.jaymo_lang.lib.gui.control.scroll.A_JG_A_ScrollableM;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 */
public abstract class A_SWT_Scrollable<TA extends Scrollable> extends A_SWT_Control<TA> {

	private final A_JG_A_ScrollableM methods = new A_JG_A_ScrollableM() {

		@Override
		protected void scrollBarH() {
			A_SWT_Scrollable.this.pendingStyle( SWT.H_SCROLL );
		}

		@Override
		protected void scrollBarV() {
			A_SWT_Scrollable.this.pendingStyle( SWT.V_SCROLL );
		}
	};

	@Override
	protected final void jmoInit3( final CallRuntime cr ) {
		this.jmoInit4( cr );
	}

	protected abstract void jmoInit4( CallRuntime cr );

	/**
	 * °scrollBarH()Same # Enable the horizontal scrollbar.
	 * °scrollBarV()Same # Enable the vertical scrollbar.
	 */
	@Override
	protected I_Object jmoMethod3( final CallRuntime cr, final String method ) {
		final I_Object result = this.methods.call( cr, method, this );

		return result != null
			? result
			: this.jmoMethod4( cr, method );
	}

	protected abstract I_Object jmoMethod4( CallRuntime cr, String method );

	@Override
	protected final void swtInit3( final CallRuntime cr ) {
		this.swtInit4( cr );
	}

	protected abstract void swtInit4( CallRuntime cr );

//	final ScrollBar h = this.swt().getHorizontalBar();
//	h.setIncrement(10);
//	h.setPageIncrement(50);

}

