/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget.control.scroll.composite.canvas.deco;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.jaymo_lang.ext.swt.core.I_SWT_Object;
import org.jaymo_lang.ext.swt.core.I_SWT_Frame;
import org.jaymo_lang.ext.swt.dialog.SWT_Error;
import org.jaymo_lang.ext.swt.widget.control.A_SWT_Control;
import org.jaymo_lang.model.App;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;
import de.mn77.base.event.Procedure;


/**
 * @author Michael Nitsche
 * @created 12.02.2010
 */
public abstract class A_SWT_Shell extends A_SWT_Control<Shell> implements I_SWT_Frame {

//	public abstract void eEndeFenster(S_Extern<?, Boolean> e);


//	public abstract void start(boolean sleak);
//	public abstract void start(boolean sleak, S_Zuhoerer<Object> bereit/*, POSITION position*/);

	public void start( final CallRuntime cr ) {
		this.start( cr, null );
	}

	public void start( final CallRuntime cr, final Procedure bereit ) {
		final Display display = /* sleak ? iSleakDisplay() : */ new Display();

		try {
			this.createSWT( cr, display, null );
			this.swt().layout();
			if( bereit != null )
				bereit.execute();
			this.iMainLoop( cr, this );
//			this.swt().dispose();
			display.dispose();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	private void iMainLoop( final CallRuntime cr, final I_SWT_Object<Shell> window ) {
		final App app = cr.getApp();
		final Display display = window.swt().getDisplay();

		try {
			while( !window.swt().isDisposed() && !app.toBeTerminated() )
				if( !display.readAndDispatch() )
					display.sleep();
		}
		catch( final Throwable t ) {
			// Sicherheitshalber erstmal auf Konsole ausgeben:
			Err.show( t );

			/*
			 * EXTREM WICHTIGE ZEILE!
			 * WAS SOLL BEI PROGRAMMFEHLERN PASSIEREN?
			 * Bis 03.03.2009 wurde die Anwendung bei Programmfehlern direkt beendet!
			 * Aber es sollte eigentlich so sein, daß die Anwendung weiterläuft und nur
			 * bei groben Fehlern, gezielt direkt im Code, beendet wird
			 */
			SWT_Error.show( cr, t ); //FIXME Hatte schon, dass zwar der Fehlerdialog kam, allerdings war er nicht anklickbar und die Anwendung war eingefroren! Problem, der SWT-Display-Thread läuft ja auch nicht mehr!
		}
	}

}
