/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget.control;

import java.awt.MouseInfo;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Widget;
import org.jaymo_lang.ext.swt.utils.SWT_ColorManager;
import org.jaymo_lang.ext.swt.utils.SWT_FontManager;
import org.jaymo_lang.ext.swt.widget.A_SWT_Widget;
import org.jaymo_lang.lib.font.JMo_Font;
import org.jaymo_lang.lib.graphic.A_ColorModel;
import org.jaymo_lang.lib.graphic.Util_Color;
import org.jaymo_lang.lib.gui.control.A_JG_A_ControlI;
import org.jaymo_lang.lib.gui.control.A_JG_A_ControlM;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 */
public abstract class A_SWT_Control<TA extends Control> extends A_SWT_Widget<TA> {

	private final A_JG_A_ControlM methods = new A_JG_A_ControlM() {

		@Override
		protected void border() {
			A_SWT_Control.this.pendingStyle( SWT.BORDER );
		}

		@Override
		protected void focus() {
			A_SWT_Control.this.pending( () -> A_SWT_Control.this.swt().setFocus() );
		}

		@Override
		protected int[] getBackground() {
			final Color backColor = A_SWT_Control.this.swt().getBackground();
			final int[] backRGB = { backColor.getRed(), backColor.getGreen(), backColor.getBlue() };
			backColor.dispose();
			return backRGB;
		}

		@Override
		protected int[] getForeground() {
			final Color foreColor = A_SWT_Control.this.swt().getForeground();
			final int[] foreRGB = { foreColor.getRed(), foreColor.getGreen(), foreColor.getBlue() };
			foreColor.dispose();
			return foreRGB;
		}

		@Override
		protected boolean hasFocus() {
			return A_SWT_Control.this.swt().isFocusControl();
		}

		@Override
		protected boolean isEnabled() {
			return A_SWT_Control.this.swt().getEnabled();
		}

		@Override
		protected boolean isMouseOver() {
			final java.awt.Point p = MouseInfo.getPointerInfo().getLocation();
			return A_SWT_Control.this.swt().getBounds().contains( A_SWT_Control.this.swt().toControl( p.x, p.y ) );
		}

		@Override
		protected boolean isVisible() {
			return A_SWT_Control.this.swt().getVisible();
		}

		@Override
		protected void setBackgroundColor( CallRuntime cr, A_ColorModel color ) {
			final int[] rgb = Util_Color.colorToRGB( color );
			this.setBackgroundRGB(cr, rgb);
		}

		@Override
		protected void setBackgroundNone( CallRuntime cr ) {
			A_SWT_Control.this.pending( () -> A_SWT_Control.this.swt().setBackground( null ) );
		}

		@Override
		protected void setBackgroundRGB( CallRuntime cr, int[] rgb ) {
			A_SWT_Control.this.pending( () -> {
				final Color c = SWT_ColorManager.getColor( rgb );
				A_SWT_Control.this.swt().setBackground( c );
			} );
		}

		@Override
		protected void setEnabled( boolean b ) {
			A_SWT_Control.this.pending( () -> A_SWT_Control.this.swt().setEnabled( b ) );
		}

		@Override
		protected void setFont( CallRuntime cr, JMo_Font fontObj ) {
			A_SWT_Control.this.pending( () -> {
				final Font font = SWT_FontManager.getFont( A_SWT_Control.this.swt().getDisplay(), fontObj );
				A_SWT_Control.this.swt().setFont( font );
			} );
		}

		@Override
		protected void setForegroundColor( CallRuntime cr, A_ColorModel color ) {
			final int[] rgb = Util_Color.colorToRGB( color );
			this.setForegroundRGB(cr, rgb);
		}

		@Override
		protected void setForegroundNone( CallRuntime cr ) {
			A_SWT_Control.this.pending( () -> A_SWT_Control.this.swt().setForeground( null ) );
		}

		@Override
		protected void setForegroundRGB( CallRuntime cr, int[] rgb ) {
			A_SWT_Control.this.pending( () -> {
				final Color c = SWT_ColorManager.getColor( rgb );
				A_SWT_Control.this.swt().setForeground( c );
			} );
		}

		@Override
		protected void setToolTip( CallRuntime cr, String toolTip ) {
			A_SWT_Control.this.pending( () -> A_SWT_Control.this.swt().setToolTipText( toolTip ) );
		}

		@Override
		protected void setVisible( boolean b ) {
			A_SWT_Control.this.pending( () -> A_SWT_Control.this.swt().setVisible( b ) );
		}
	};

	private final A_JG_A_ControlI inits = new A_JG_A_ControlI() {

		@Override
		protected void atKey( final CallRuntime cr, String ev ) {
			A_SWT_Control.this.registerEvent( ev, () -> {
				A_SWT_Control.this.swt().addListener( SWT.KeyUp, event -> A_SWT_Control.this.eventRun( cr, "@key", A_SWT_Control.this ) );
			} );
		}
		@Override
		protected void atKeyUp( final CallRuntime cr, String ev ) {
			A_SWT_Control.this.registerEvent( ev, () -> {
				A_SWT_Control.this.swt().addListener( SWT.KeyUp, event -> A_SWT_Control.this.eventRun( cr, "@keyUp", A_SWT_Control.this ) );
			} );
		}
		@Override
		protected void atMouseClick( final CallRuntime cr, String ev ) {
			A_SWT_Control.this.registerEvent( ev, () -> {
				A_SWT_Control.this.swt().addListener( SWT.MouseUp, event -> A_SWT_Control.this.eventRun( cr, "@mouseClick", A_SWT_Control.this ) ); // or SWT.MouseDown
			} );
		}
		@Override
		protected void atMouseDoubleClick( final CallRuntime cr, String ev ) {
			A_SWT_Control.this.registerEvent( ev, () -> {
				A_SWT_Control.this.swt().addListener( SWT.MouseDoubleClick, event -> A_SWT_Control.this.eventRun( cr, "@mouseDoubleClick", A_SWT_Control.this ) );
			} );
		}
		@Override
		protected void atMouseUp( final CallRuntime cr, String ev ) {
			A_SWT_Control.this.registerEvent( ev, () -> {
				A_SWT_Control.this.swt().addListener( SWT.MouseUp, event -> A_SWT_Control.this.eventRun( cr, "@mouseUp", A_SWT_Control.this ) ); // or SWT.MouseDown
			} );
		}
		@Override
		protected void atMove( final CallRuntime cr, String ev ) {
			A_SWT_Control.this.registerEvent( ev, () -> {
				A_SWT_Control.this.swt().addListener( SWT.Move, event -> A_SWT_Control.this.eventRun( cr, "@move", A_SWT_Control.this ) );
			} );
		}
		@Override
		protected void atResize( final CallRuntime cr, String ev ) {
			A_SWT_Control.this.registerEvent( ev, () -> {
				A_SWT_Control.this.swt().addListener( SWT.Resize, event -> A_SWT_Control.this.eventRun( cr, "@resize", A_SWT_Control.this ) );
			} );
		}
	};

	/**
	 * °@mouseClick # On mouse click
	 * °@mouseUp
	 * °@mouseDoubleClick # On mouse double click
	 *
	 * °@key # On key relesed
	 * °@keyUp # On key relesed
	 *
	 * °@move # On widget moved
	 * °@resize # On widged resize
	 */
	@Override
	protected final void jmoInit2( final CallRuntime cr ) {
		this.inits.init(cr, this);
		this.jmoInit3( cr );
	}

	protected abstract void jmoInit3( CallRuntime cr );

	/**
	 * °border()Same # Enable and draw border for this widget.
	 * °setTip ^ setToolTip
	 * °setToolTip(Str top)Same # Set the tooltip for this widget.
	 * °isEnabled()Bool # Returns true, if this widget is enabled.
	 * °isVisible()Bool # Returns true, if this widget is visible.
	 * °setEnabled(Bool b)Same # Enable or disable this widget.
	 * °setVisible(Bool b)Same # Show or hide this widget.
	 * °getBackground()ColorRGB # Returns the defined background color.
	 * °getForeground()ColorRGB # Returns the defined foreground color.
	 * °isMouseOver()Bool # Returns true, if the mouse cursor is over this widget.
	 * °focus()Same # Set the focus on this widget.
	 *
	 * °setBackground(Color color)Same # Set the background color of this widget.
	 * °setBackground(Int red, Int green, Int blue)Same # Set the background color of this widget.
	 * °setFont(SWT_Font font)Same # Set the font for this widget.
	 * °setForeground(Color color)Same # Set the background color of this widget.
	 * °setForeground(Int red, Int green, Int blue)Same # Set the background color of this widget.
	 */
	@Override
	protected final I_Object jmoMethod2( final CallRuntime cr, final String method ) {
//		switch( method ) {
//			case "getBounds":
//				return Bool.getObject(this.swt().getBounds());
//			case "getSize":
//				return Bool.getObject(this.swt().getSize());
//		}

		final I_Object result = this.methods.call( cr, method, this );

		return result != null
			? result
			: this.jmoMethod3( cr, method );
	}

	protected abstract I_Object jmoMethod3( CallRuntime cr, String method );

	protected abstract TA swtCreate( Composite base, int style );

	@Override
	protected final TA swtCreate( final Widget parent, final int style ) {
		return this.swtCreate( (Composite)parent, style );
	}

	@Override
	protected final void swtInit2( final CallRuntime cr ) {
		this.swtInit3( cr );
	}

	protected abstract void swtInit3( CallRuntime cr );

}

