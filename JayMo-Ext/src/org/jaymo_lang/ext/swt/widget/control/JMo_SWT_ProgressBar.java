/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget.control;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ProgressBar;
import org.jaymo_lang.lib.gui.control.A_JG_ProgressBarI;
import org.jaymo_lang.lib.gui.control.A_JG_ProgressBarM;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.AXIS;


/**
 * @author Michael Nitsche
 */
public class JMo_SWT_ProgressBar extends A_SWT_Control<ProgressBar> {

	private final ArgCallBuffer arg0;

	/*
	 * °setRange(Range range)Same # Set minimum and maximum
	 * °setRange(Int min, Int max)Same # Set minimum and maximum
	 * °setValue(IntNumber num)Same # Set the value of the progress bar.
	 * °getMin()Int # Returns the current value.
	 * °getMax()Int # Returns the current value.
	 * °getValue()Int # Returns the current value.
	 * °getRange()Range # Returns the current range from min to max.
	 * °next ^ inc
	 * °inc()Same # Push the value one step forward
	 * °hasNext()Bool # Returns true, if value lower than max.
	 */
	private final A_JG_ProgressBarM methods = new A_JG_ProgressBarM() {

		@Override
		protected void setRange( final int min, final int max ) {
			// Set it two times!
			JMo_SWT_ProgressBar.this.pending( () -> {
				for( int i = 1; i <= 2; i++ ) {
					JMo_SWT_ProgressBar.this.swt().setMinimum( min );
					JMo_SWT_ProgressBar.this.swt().setMaximum( max );
					JMo_SWT_ProgressBar.this.swt().setSelection( min );
				}
			} );
		}

		@Override
		protected void setValue( int value ) {
			JMo_SWT_ProgressBar.this.pending( () -> {
				JMo_SWT_ProgressBar.this.swt().setSelection( value );
			} );
		}

		@Override
		protected int getMinimum() {
			return 0;
		}

		@Override
		protected int getMaximum() {
			return 0;
		}

		@Override
		protected int getValue() {
			return 0;
		}
	};

	private final A_JG_ProgressBarI inits = new A_JG_ProgressBarI() {

		@Override
		protected void arg0align( AXIS align ) {
			if( align == AXIS.X )
				JMo_SWT_ProgressBar.this.pendingStyle( SWT.HORIZONTAL );
			else
				JMo_SWT_ProgressBar.this.pendingStyle( SWT.VERTICAL );
		}
	};

	/**
	 * +SWT_ProgressBar(MagicAxis xy)
	 */
	public JMo_SWT_ProgressBar( final Call arg ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
	}


	@Override
	public void jmoInit3( final CallRuntime cr ) {
		this.inits.init(cr, this, this.arg0);
	}

	/**
	 * °getMin()Int # Returns the defined minimum value
	 * °getMax()Int # Returns the defined maximum value
	 * °getValue()Int # Returns the current value
	 * °getRange()Range # Returns the current range
	 *
	 * °hasNext()Bool | Returns true if the current value is less than the defined maximum value.
	 *
	 * °inc ^ next
	 * °next()Int # Increment value by 1.
	 *
	 * °setRange(Range range)Same # Set minimum and maximum
	 * °setRange(Int min, Int max)Same # Set minimum and maximum
	 * °setValue(Int value)Same # Set value
	 */
	@Override
	protected I_Object jmoMethod3( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	@Override
	protected ProgressBar swtCreate( final Composite parent, final int style ) {
		return new ProgressBar( parent, style );
	}

	@Override
	protected void swtInit3( final CallRuntime cr ) {}

}
