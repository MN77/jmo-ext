/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget.control;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.jaymo_lang.ext.swt.graphic.SWT_Image;
import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.gui.control.A_JG_ButtonI;
import org.jaymo_lang.lib.gui.control.A_JG_ButtonM;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @implNote complete
 */
public class JMo_SWT_Button extends A_SWT_Control<Button> {

	private final ArgCallBuffer arg0;

	private final A_JG_ButtonM methods = new A_JG_ButtonM() {

		@Override
		protected I_Object getImage() {
			final Image img = JMo_SWT_Button.this.swt().getImage();
			return img == null
				? Nil.NIL
				: new SWT_Image( img ).toJayMoImage();
		}

		@Override
		protected String getText() {
			return JMo_SWT_Button.this.swt().getText(); // String will be maybe empty
		}

		@Override
		protected void setImage( JMo_Image image ) {
			if(image == null)
				JMo_SWT_Button.this.pending( () -> JMo_SWT_Button.this.swt().setImage( null ) ); // testen!
			else
				JMo_SWT_Button.this.pending( () -> JMo_SWT_Button.this.swt().setImage( new SWT_Image( image ).getImage() ) );
		}

		@Override
		protected void setText( String text ) {
			JMo_SWT_Button.this.pending( () -> JMo_SWT_Button.this.swt().setText( text ) );
		}
	};

	private final A_JG_ButtonI inits = new A_JG_ButtonI() {
		@Override
		protected void atSelect(CallRuntime cr, String ev) {
			JMo_SWT_Button.this.registerEvent( ev, () -> {
				JMo_SWT_Button.this.swt().addListener( SWT.Selection, event -> JMo_SWT_Button.this.eventRun( cr, ev, JMo_SWT_Button.this ) );
			} );
		}

		@Override
		protected void arg0str( JMo_Str s ) {
			JMo_SWT_Button.this.pending( () -> {
				JMo_SWT_Button.this.swt().setText( s.rawString() );
			} );
		}

		@Override
		protected void arg0image( JMo_Image s ) {
			JMo_SWT_Button.this.pending( () -> {
				JMo_SWT_Button.this.swt().setImage( SWT_Image.newDirect( s.internalGet() ).getImage() );
			} );
		}
	};


	/**
	 * +SWT_Button()
	 * +SWT_Button(Image img)
	 * +SWT_Button(Str text)
	 */
	public JMo_SWT_Button() {
		this.arg0 = null;
		this.pendingStyle( SWT.PUSH );
	}

	public JMo_SWT_Button( final Call arg ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
		this.pendingStyle( SWT.PUSH );
	}


	/**
	 * °@select # On button select
	 */
	@Override
	protected void jmoInit3( final CallRuntime cr ) {
		this.inits.init(cr, this, this.arg0);
	}

	/**
	 * °setText(Str text)Same # Set the message for this button
	 * °getText()Str # Returns the message on this button
	 * °setImage(Image image)Same # Set the image for this button
	 * °getImage()Image? # Returns the image of this button or nil if not set.
	 */
	@Override
	public I_Object jmoMethod3( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	@Override
	protected Button swtCreate( final Composite parent, final int style ) {
		return new Button( parent, style );
	}

	@Override
	protected void swtInit3( final CallRuntime cr ) {}

}
