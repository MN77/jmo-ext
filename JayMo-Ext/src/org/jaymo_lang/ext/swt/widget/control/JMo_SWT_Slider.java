/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget.control;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Slider;
import org.jaymo_lang.lib.gui.control.A_JG_SliderI;
import org.jaymo_lang.lib.gui.control.A_JG_SliderM;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.AXIS;


/**
 * @author Michael Nitsche
 */
public class JMo_SWT_Slider extends A_SWT_Control<Slider> {

	private static final int    STEPS = 10;
	private final ArgCallBuffer arg0;

	private final A_JG_SliderM methods = new A_JG_SliderM() {

		@Override
		protected void setRange( final int min, final int max ) {
			JMo_SWT_Slider.this.pending( () -> {

				JMo_SWT_Slider.this.pending( () -> {
					final int thumb = Math.max( 1, Math.round( (max - min) / JMo_SWT_Slider.STEPS ) );
					JMo_SWT_Slider.this.swt().setValues( min, min, max, thumb, 1, thumb );
				} );
			} );
		}

		@Override
		protected void setValue( final int value ) {
			JMo_SWT_Slider.this.pending( () -> {
				JMo_SWT_Slider.this.swt().setSelection( value );
			} );
		}

		@Override
		protected int getMinimum() {
			return JMo_SWT_Slider.this.swt().getMinimum();
		}

		@Override
		protected int getMaximum() {
			return JMo_SWT_Slider.this.swt().getMaximum();
		}

		@Override
		protected int getValue() {
			return JMo_SWT_Slider.this.swt().getSelection();
		}
	};

	private final A_JG_SliderI inits = new A_JG_SliderI() {

		@Override
		protected void arg0align( AXIS align ) {
			if( align == AXIS.X )
				JMo_SWT_Slider.this.pendingStyle( SWT.HORIZONTAL );
			else
				JMo_SWT_Slider.this.pendingStyle( SWT.VERTICAL );
		}

		@Override
		protected void atChange( CallRuntime cr, String ev ) {
			JMo_SWT_Slider.this.registerEvent( ev, () -> {
				JMo_SWT_Slider.this.swt().addListener( SWT.Selection, event -> JMo_SWT_Slider.this.eventRun( cr, ev, JMo_SWT_Slider.this ) );
			} );
		}
	};


	/**
	 * +SWT_Slider(MagicAxis xy)
	 */
	public JMo_SWT_Slider( final Call arg ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
	}


	@Override
	public void jmoInit3( final CallRuntime cr ) {
		this.inits.init(cr, this, this.arg0);

//		this.registerEvent("@change", ()-> {
////			this.swt().addListener(SWT.change, event-> A_SWT_Control.this.eventRun(cr, "@mouseClick", A_SWT_Control.this)); // or SWT.MouseDown
//
//			this.swt().addSelectionListener(new SelectionListener() {
//
//				public void widgetDefaultSelected(final SelectionEvent e) {
//					JMo_SWT_Slider.this.eventRun(cr, "@change", JMo_SWT_Slider.this);
//				}
//
//				public void widgetSelected(final SelectionEvent e) {
//					JMo_SWT_Slider.this.eventRun(cr, "@change", JMo_SWT_Slider.this);
//				}
//
//			});
//		});
	}

	/**
	 * °getMin()Int # Returns the defined minimum value
	 * °getMax()Int # Returns the defined maximum value
	 * °getValue()Int # Returns the current value
	 * °getRange()Range # Returns the current range
	 * °setRange(Range range)Same # Set minimum and maximum
	 * °setRange(Int min, Int max)Same # Set minimum and maximum
	 * °setValue(Int value)Same # Set value
	 */
	@Override
	protected I_Object jmoMethod3( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	@Override
	protected Slider swtCreate( final Composite parent, final int style ) {
		return new Slider( parent, style );
	}

	@Override
	protected void swtInit3( final CallRuntime cr ) {}

}
