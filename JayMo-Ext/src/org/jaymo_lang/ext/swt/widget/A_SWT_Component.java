/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.swt.widgets.Widget;
import org.jaymo_lang.ext.swt.core.A_SWT_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.event.Procedure;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 */
public abstract class A_SWT_Component<TA> extends A_SWT_Object<TA> {

	private ArrayList<Procedure> pendingProc  = null;
	private int                  pendingStyle = 0; // SWT.NONE
	private TA                   swt;

	// One way is, to register all events. The other way is, to only register used events ==> better
	private final HashMap<String, Procedure> eventMap      = new HashMap<>();
	private final ArrayList<String>          pendingEvents = new ArrayList<>();


	/**
	 * @implNote "parent" should be Composite, but that deters TreeItem>TreeItem>TreeItem
	 */
	public final TA createSWT( final CallRuntime cr, final Widget parent ) {
		this.swt = this.swtCreate( parent, this.pendingStyle );
		Err.ifNull( 1, this.swt );
		if( this.swt instanceof Widget )
			((Widget)this.swt).setData( this );
		else
			MOut.warning( "Kann setData nicht aufrufen!", this.swt.getClass() );

		if( this.pendingProc != null )
			for( final Procedure v : this.pendingProc )
				v.execute();
		this.swtInit1( cr );

		for( final String event : this.pendingEvents ) {
			final Procedure proc = this.eventMap.get( event );

			if( proc != null ) {
				proc.execute();
				this.eventMap.put( event, null );
			}
		}

		return this.swt;
	}

	@Override
	public final void init( final CallRuntime cr ) {
		this.jmoInit1( cr );
	}

	public void pendingStyle( final int style ) {
		if( this.isCreated() )
//			throw new ExecError(cr, "SWT-Widget already created", "This function can only be set before the Main.run function.");
			throw new RuntimeException( "SWT-Widget already created. This function can only be set before the Main.run function." );
		else
			this.pendingStyle = this.pendingStyle | style;
	}

	public TA swt() {
		if( this.swt == null )
			throw new Err_Runtime( "SWT-Object isn't created yet!" );
		return this.swt;
	}

	@Override
	public final boolean validateEvent( final String event ) {
		final boolean exist = this.eventMap.containsKey( event );

		if( this.swt == null )
			this.pendingEvents.add( event );
		else {
			final Procedure proc = this.eventMap.get( event );

			if( proc != null ) {
				proc.execute();
				this.eventMap.put( event, null );
			}
		}
		return exist;
	}

	@Override
	protected final I_Object callMethod( final CallRuntime cr, final String method ) {
		return this.jmoMethod1( cr, method );
	}

	@Override
	protected boolean isCreated() {
		return this.swt != null;
	}

	protected abstract void jmoInit1( CallRuntime cr );
	protected abstract I_Object jmoMethod1( CallRuntime cr, String method );


	// CREATE SWT

	protected void pending( final Procedure v ) {

		if( this.swt != null )
			v.execute();
		else {
			if( this.pendingProc == null )
				this.pendingProc = new ArrayList<>();
			this.pendingProc.add( v );
		}
	}

	protected void registerEvent( final String event, final Procedure add ) {
		this.eventMap.putIfAbsent( event, add );
	}

	//	protected abstract int swtStyle1();
	protected abstract TA swtCreate( Widget parent, int style );

	protected abstract void swtInit1( CallRuntime cr );

}

