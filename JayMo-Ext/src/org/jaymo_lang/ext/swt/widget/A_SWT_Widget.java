/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget;

import org.eclipse.swt.widgets.Widget;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 */
public abstract class A_SWT_Widget<TA extends Widget> extends A_SWT_Component<TA> {

	protected abstract I_Object jmoMethod2( CallRuntime cr, String method );

	@Override
	protected final void jmoInit1( final CallRuntime cr ) {
		//TODO register @select here?!?!?
//		this.registerEvent("@select", ()-> {
//			this.swt().addListener(SWT.Selection, event-> A_SWT_Widget.this.eventRun(cr, "@select", A_SWT_Widget.this));
//		});

//		TODO @dispose
//		this.swt().addListener(SWT.Dispose, event -> {});

		this.jmoInit2( cr );
	}

	protected abstract void jmoInit2( CallRuntime cr );

	/**
	 * °dispose()Nil # Dispose this widget
	 * °isDisposed()Bool # Returns true, if this widget is disposed
	 */
	@Override
	protected final I_Object jmoMethod1( final CallRuntime cr, final String method ) {

		switch( method ) {
			// TODO:
//			public void eFreigabe(final Listener l) {
//				this.swt().addListener(SWT.Dispose, l);
//			}
//			public void eSelect(final Listener l) {
//				this.swt().addListener(SWT.Selection, l);
//			}

			case "dispose":
				cr.argsNone();
				this.swt().dispose();
				return Nil.NIL;
			case "isDisposed":
				cr.argsNone();
				return JMo_Bool.getObject( this.swt().isDisposed() );
		}

		return this.jmoMethod2( cr, method );
	}

	@Override
	protected final void swtInit1( final CallRuntime cr ) {
		this.swtInit2( cr );
	}

	protected abstract void swtInit2( CallRuntime cr );

}
