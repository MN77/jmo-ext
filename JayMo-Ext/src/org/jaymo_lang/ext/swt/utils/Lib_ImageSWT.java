/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.utils;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;

import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;


public class Lib_ImageSWT {

	public static ImageData convert_AWT_to_SWT( final BufferedImage bufferedimage ) {

		if( bufferedimage.getColorModel() instanceof DirectColorModel ) {
			final DirectColorModel colormodel = (DirectColorModel)bufferedimage.getColorModel();
			final PaletteData palette = new PaletteData( colormodel.getRedMask(), colormodel.getGreenMask(), colormodel.getBlueMask() );
			final ImageData data = new ImageData( bufferedimage.getWidth(), bufferedimage.getHeight(), colormodel.getPixelSize(), palette );
			final WritableRaster raster = bufferedimage.getRaster();
			final boolean hasAlpha = bufferedimage.getColorModel().hasAlpha();
			final int[] pixelArray = new int[hasAlpha ? 4 : 3];

			for( int y = 0; y < data.height; y++ )
				for( int x = 0; x < data.width; x++ ) {
					raster.getPixel( x, y, pixelArray );
					final int pixel = palette.getPixel( new RGB( pixelArray[0], pixelArray[1], pixelArray[2] ) );
					data.setPixel( x, y, pixel );
					if( hasAlpha )
						data.setAlpha( x, y, pixelArray[3] );
				}

			return data;
		}
		else if( bufferedimage.getColorModel() instanceof IndexColorModel ) {
			final IndexColorModel colorModel = (IndexColorModel)bufferedimage.getColorModel();
			final int size = colorModel.getMapSize();
			final byte[] reds = new byte[size];
			final byte[] greens = new byte[size];
			final byte[] blues = new byte[size];
			colorModel.getReds( reds );
			colorModel.getGreens( greens );
			colorModel.getBlues( blues );
			final RGB[] rgbs = new RGB[size];

			for( int i = 0; i < rgbs.length; i++ )
				rgbs[i] = new RGB( reds[i] & 0xFF, greens[i] & 0xFF, blues[i] & 0xFF );

			final PaletteData palette = new PaletteData( rgbs );
			final ImageData data = new ImageData( bufferedimage.getWidth(), bufferedimage.getHeight(), colorModel.getPixelSize(), palette );
			data.transparentPixel = colorModel.getTransparentPixel();
			final WritableRaster raster = bufferedimage.getRaster();
			final int[] pixelArray = new int[1];

			for( int y = 0; y < data.height; y++ )
				for( int x = 0; x < data.width; x++ ) {
					raster.getPixel( x, y, pixelArray );
					data.setPixel( x, y, pixelArray[0] );
				}

			return data;
		}

		return null;
	}

//	private ImageData iConvertToSWT(final BufferedImage bufferedImage) {
//		final DirectColorModel colorModel = (DirectColorModel)bufferedImage.getColorModel();
//		final PaletteData palette = new PaletteData(colorModel.getRedMask(), colorModel.getGreenMask(), colorModel.getBlueMask());
//		final ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(), colorModel.getPixelSize(), palette);
//
//		int rgb = 0; // Use a single variable!
//
//		for(int y = 0; y < data.height; y++)
//			for(int x = 0; x < data.width; x++) {
//				rgb = bufferedImage.getRGB(x, y);
////				int pixel = palette.getPixel(new RGB(rgb >> 16 & 0xFF, rgb >> 8 & 0xFF, rgb & 0xFF));
//				data.setPixel(x, y, rgb);
//
////				if(colorModel.hasAlpha())
////					data.setAlpha(x, y, rgb >> 24 & 0xFF);
//			}
//		return data;
//	}

	public static BufferedImage convert_SWT_to_AWT( final ImageData data ) {
		ColorModel colormodel = null;
		final PaletteData palette = data.palette;

		if( palette.isDirect ) {
			colormodel = new DirectColorModel( data.depth, palette.redMask, palette.greenMask, palette.blueMask );
//			BufferedImage bufferedImage = new BufferedImage(colormodel, colormodel.createCompatibleWritableRaster(data.width, data.height), false, null);
			final BufferedImage bufferedImage = new BufferedImage( data.width, data.height, BufferedImage.TYPE_INT_ARGB );
			final WritableRaster raster = bufferedImage.getRaster();
			final int[] pixelArray = new int[4];

			for( int y = 0; y < data.height; y++ )
				for( int x = 0; x < data.width; x++ ) {
					final int pixel = data.getPixel( x, y );
					final RGB rgb = palette.getRGB( pixel );
					pixelArray[3] = data.getAlpha( x, y );
					pixelArray[0] = rgb.red;
					pixelArray[1] = rgb.green;
					pixelArray[2] = rgb.blue;
					raster.setPixels( x, y, 1, 1, pixelArray );
				}

			return bufferedImage;
		}
		else {
			final RGB[] rgbs = palette.getRGBs();
			final byte[] red = new byte[rgbs.length];
			final byte[] green = new byte[rgbs.length];
			final byte[] blue = new byte[rgbs.length];

			for( int i = 0; i < rgbs.length; i++ ) {
				final RGB rgb = rgbs[i];
				red[i] = (byte)rgb.red;
				green[i] = (byte)rgb.green;
				blue[i] = (byte)rgb.blue;
			}

			if( data.transparentPixel != -1 )
				colormodel = new IndexColorModel( data.depth, rgbs.length, red, green, blue, data.transparentPixel );
			else
				colormodel = new IndexColorModel( data.depth, rgbs.length, red, green, blue );

			final BufferedImage bufferedimage = new BufferedImage( colormodel, colormodel.createCompatibleWritableRaster( data.width, data.height ), false, null );
			final WritableRaster raster = bufferedimage.getRaster();
			final int[] pixelArray = new int[1];

			for( int y = 0; y < data.height; y++ )
				for( int x = 0; x < data.width; x++ ) {
					final int pixel = data.getPixel( x, y );
					pixelArray[0] = pixel;
					raster.setPixel( x, y, pixelArray );
				}

			return bufferedimage;
		}
	}

}
