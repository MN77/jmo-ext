/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.utils;

import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.jaymo_lang.lib.font.JMo_Font;


/**
 * @author Michael Nitsche
 *         18.01.2010 Erstellt
 */
public class SWT_FontManager {

	private static HashMap<String, Font> fonts = new HashMap<>();


//	public static Font getFont(final Device device, final SWT_SCHRIFT font, final Integer size, final SWT_FONTSTYLE... styles) {
//		return SWT_FontManager.iFont(device, font, size, styles);
//	}

	public static Font getFont( final Device device, JMo_Font font ) {
		final String name = font.getName();
		final int size = font.getSize();

		int style = 0;
		if( font.isBold() )
			style = style | SWT.BOLD;
		if( font.isItalic() )
			style = style | SWT.ITALIC;

		final FontData data = new FontData( name, size, style );
		return SWT_FontManager.getFont( device, data );
	}

	public static Font getFont( final Device device, final FontData data ) {
		return SWT_FontManager.iFont( device, data );
	}

//	public static Font getFontFromJar(final Device device, final String jarpath, final String filename, final String suffix, final String fontname, final Integer size, final SWT_FONTSTYLE... styles) throws Err_FileSys, IOException {
//		final File temp = Lib_Jar.getFileCopy(jarpath, filename, suffix);
//		return SWT_FontManager.getFontFromFile(device, temp.getAbsolutePath(), fontname, size, styles);
//	}

//	public static Font getFontFromFile(final Device device, final String fullpath, final String fontname, final Integer size, final SWT_FONTSTYLE... styles) throws Err_FileSys, IOException {
//		//TODO: Diesen Block in eigene Methode!
//		final String key = SWT_FontManager.iKey(device, fontname, size, styles);
//		if(SWT_FontManager.fonts.getKeys().contains(key)) {
//			final Font font = SWT_FontManager.fonts.get(key);
//			if(font.isDisposed())
//				SWT_FontManager.fonts.remove(key);
//			else
//				return font;
//		}
//
////		boolean fontIsLoaded = Display.getCurrent().loadFont("/usr/share/fonts/truetype/mike-alt/ried.ttf");
//		final boolean fontIsLoaded = Display.getCurrent().loadFont(fullpath);
//		if(!fontIsLoaded)
//			new Err_FileSys("Datei nicht geladen!", fontIsLoaded, fullpath);
//		final Font font = new Font(null, fontname, size, SWT_FONTSTYLE.stile2Nr(styles));
////		Font font = new Font(null,"Ried",12,SWT.BOLD);
//		SWT_FontManager.fonts.add(key, font);
//		return font;
//	}

	// INTERN

	private static Font iCreateFont( final String schluessel, final Device device, final FontData data ) {
		final Font font = new Font( device, data );
		SWT_FontManager.fonts.put( schluessel, font );
		return font;
	}

//	private static Font iFont(final Device device, final SWT_SCHRIFT schrift, final Integer groesse, final SWT_FONTSTYLE... stile) {
//		final String schluessel = SWT_FontManager.iKey(device, schrift, groesse, stile);
//		if(SWT_FontManager.fonts.getKeys().contains(schluessel)) {
//			Font font = SWT_FontManager.fonts.get(schluessel);
//			if(font.isDisposed()) {
//				SWT_FontManager.fonts.remove(schluessel);
//				font = SWT_FontManager.iNeu(schluessel, device, schrift, groesse, stile);
//			}
//			return font;
//		}
//		else
//			return SWT_FontManager.iNeu(schluessel, device, schrift, groesse, stile);
//	}

//	private static Font iNeu(final String schluessel, final Device device, SWT_SCHRIFT schrift, Integer groesse, final SWT_FONTSTYLE[] stile) {
//		int style = 0; //==SWT_SCHRIFTSTIL.NORMAL.swt;
//		if(stile != null)
//			for(final SWT_FONTSTYLE stil : stile)
//				style = style | stil.swt;
//
//		if(schrift == null)
//			schrift = SWT_SCHRIFT.STANDARD;
//
//		if(groesse == null)
//			groesse = 12; //Standard in FontData
//
//		final FontData data = new FontData(schrift.schrift, groesse, style);
//		return SWT_FontManager.iCreateFont(schluessel, device, data);
//	}

	private static Font iFont( final Device device, final FontData data ) {
		final String key = SWT_FontManager.iKey( device, data );

		if( SWT_FontManager.fonts.keySet().contains( key ) ) {
			Font font = SWT_FontManager.fonts.get( key );

			if( font.isDisposed() ) {
				SWT_FontManager.fonts.remove( key );
				font = SWT_FontManager.iCreateFont( key, device, data );
			}
			return font;
		}
		else
			return SWT_FontManager.iCreateFont( key, device, data );
	}

//	private static String iKey(final Device d, final SWT_SCHRIFT schrift, final Integer groesse, final SWT_FONTSTYLE[] stile) {
//		return SWT_FontManager.iKey(d, schrift.toString(), groesse, stile);
//	}

//	private static String iKey(final Device d, final String schrift, final Integer groesse, final SWT_FONTSTYLE[] stile) {
//		return SWT_FontManager.iNull(d) + "," + SWT_FontManager.iNull(schrift) + "," + SWT_FontManager.iNull(groesse) + "," + SWT_FontManager.iStylesToString(stile);
//	}

	private static String iKey( final Device d, final FontData data ) {
		return SWT_FontManager.iNull( d ) + "," + SWT_FontManager.iNull( data );
	}

	private static String iNull( final Object o ) {
		return o == null ? "null" : o.toString();
	}

//	private static String iStylesToString(final SWT_FONTSTYLE[] styles) {
//		if(styles == null)
//			return "null";
//		String s = "";
//		for(final SWT_FONTSTYLE style : styles)
//			s += style.toString();
//		return s;
//	}

}
