/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.marytts;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.I_Table;
import de.mn77.ext.marytts.MaryTTS;


/**
 * @author Michael Nitsche
 *         15.06.2018 Erstellt
 */
public class JMo_MaryTTS extends A_ObjectSimple {

	private String              voice = null;
	private MaryTTS             tts   = null; // TODO Only one single instance?
	private final ArgCallBuffer par_voice;


	/**
	 * + MaryTTS()
	 * + MaryTTS(Str voice)
	 * ! MaryTTS is an open-source, multilingual Text-to-Speech Synthesis platform written in Java.
	 */
	public JMo_MaryTTS() {
		this.par_voice = null;
	}

	/**
	 * + MaryTTS(Str voice)
	 */
	public JMo_MaryTTS( final Call voice ) {
		this.par_voice = new ArgCallBuffer( 0, voice );
	}

	@Override
	public void init( final CallRuntime cr ) {

		if( this.par_voice != null ) {
			final JMo_Str arg = this.par_voice.init( cr, this, JMo_Str.class );
			this.voice = arg.rawString();
		}
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "init":
			case "start":
				this.mStart( cr );
				return this;

			case "say":
			case "saySync":
				this.mSay( cr, true );
				return this;
			case "sayAsync":
				this.mSay( cr, false );
				return this;

			case "setVoice":
				this.mSetVoice( cr );
				return this;

			case "voices":
				return this.mVoices( cr );

			default:
				return null;
		}
	}

	private void iCheckStarted( final CallRuntime cr ) {
		if( this.tts == null )
			throw new RuntimeError( cr, "MaryTTS is not running!", "Please start MaryTTS first with '.start'" );	//ok
	}

	/**
	 * °say ^ saySync
	 * °saySync(Str text)Same # Speak that text and wait until it is done.
	 * °sayAsync(Str text)Same # Speak that text in a background process
	 */
	private void mSay( final CallRuntime cr, final boolean wait ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];
		this.iCheckStarted( cr );
		final String text = arg.rawString();

		if( this.voice != null ) {
			this.tts.setVoiceName( this.voice );
			this.voice = null;
		}

		this.tts.textToSpeach( text, wait );
	}

	/**
	 * °setVoice(Str voice)Same # Set a voice
	 */
	private void mSetVoice( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];
		this.voice = arg.rawString();
	}

	/**
	 * °init ^ start
	 * °start()Same # Start and initiate MaryTTS.
	 */
	private void mStart( final CallRuntime cr ) {
		cr.argsNone();

		if( this.tts != null )
			cr.warning( "MaryTTS allready started", "Can't start MaryTTS twice." );
		else {
			this.tts = new MaryTTS();
			this.tts.startService();
		}
	}

	/**
	 * °voices()Table # Get a Table with all available voices
	 */
	private I_Object mVoices( final CallRuntime cr ) {
		cr.argsNone();
		this.iCheckStarted( cr );
		final I_Table<String> voices = this.tts.getVoices();
		final ArrayTable<I_Object> voices2 = new ArrayTable<>( 2 );

		for( int z = 0; z < voices.size(); z++ ) {
			final String[] zz = voices.getRow( z );
			final I_Object l = new JMo_Str( zz[0] );
			final I_Object n = new JMo_Str( zz[1] );
			voices2.addRow( l, n );
		}
		return new JMo_Table( voices2 );
	}

}
