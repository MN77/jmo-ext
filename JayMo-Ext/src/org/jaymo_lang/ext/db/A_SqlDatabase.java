/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.db;

import java.sql.ResultSet;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err_Runtime;
import de.mn77.ext.db.sql.A_SqlDB;


/**
 * @author Michael Nitsche
 * @created 10.04.2021
 */
public abstract class A_SqlDatabase extends A_ObjectSimple {

	private A_SqlDB sqldb = null; // null = not open / not connected


	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "open": // it's the opposite of "close"
			case "connect": // to server
				this.mConnect( cr );
				return this;
			case "close":
				this.mClose( cr );
				return this;

			case "query":
				return this.mQuery( cr );
			case "exec":
				this.mExec( cr );
				return this;
			case "update":
				return this.mUpdate( cr );
		}
		return null;
	}

	// ------------------------------------------

	protected abstract A_SqlDB openDatabase( CallRuntime cr );

	private void iCheckOpen( final CallRuntime cr ) {
		if( this.sqldb == null )
			throw new RuntimeError( cr, "Database-Connection-Error", "Please open connection first!" );
	}

	/**
	 * °close()Same # Close the database
	 */
	private void mClose( final CallRuntime cr ) {
		cr.argsNone();

		if( this.sqldb == null )
			cr.warning( "Database-Error", "Database not open or already closed." );
		else {
			this.sqldb.close();
			this.sqldb = null;
		}
	}

	/**
	 * °open ^ connect
	 * °connect()Same # Open the database
	 */
	private void mConnect( final CallRuntime cr ) {
		cr.argsNone();
		if( this.sqldb != null )
			cr.warning( "Database-Error", "Database is already open." );
		else
			this.sqldb = this.openDatabase( cr );
	}

	/**
	 * °exec(Str sql)Same # Execute a SQL-Statement.
	 */
	private void mExec( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];
		this.iCheckOpen( cr );

		try {
			this.sqldb.exec( arg.rawString() );
		}
		catch( final Err_Runtime e ) {
			throw e;
		}
	}

	/**
	 * °query(Str sql)DBResult? # Execute a SQL-Statement. If the result is empty, 'nil' will be returned.
	 */
	private I_Object mQuery( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];
		this.iCheckOpen( cr );

		try {
			final ResultSet rs = this.sqldb.query( arg.rawString() );
			if( rs == null )
				return Nil.NIL;
			return new JMo_DBResult( rs, this.sqldb.getDBMS() );
		}
		catch( final Err_Runtime e ) {
			throw e;
		}
	}

	/**
	 * °update(Str sql)Int # Execute a SQL-Statement, return the amount of updates
	 */
	private JMo_Int mUpdate( final CallRuntime cr ) {
		final JMo_Str arg = cr.arg( this, JMo_Str.class );
		this.iCheckOpen( cr );

		try {
			final int result = this.sqldb.update( arg.rawString() );
			return new JMo_Int( result );
		}
		catch( final Err_Runtime e ) {
			throw e;
		}
//		catch(final Err_DataBase e) {
//			throw new ExtError(cr, "Database-Error", sql.toString() + " -> " + e.getMessage());
//		}
	}

}
