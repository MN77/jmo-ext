/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.db;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.ext.db.login.Login_HSQLDB;
import de.mn77.ext.db.sql.A_SqlDB;
import de.mn77.ext.db.sql.HSQLDB;


/**
 * @author Michael Nitsche
 * @created 21.06.2018
 */
public class JMo_HSqlDB extends A_SqlDatabase {

	private final ArgCallBuffer file;


	/**
	 * ! HSqlDB is a free relational SQL database, programmed in Java.
	 * + HSqlDB()
	 * + HSqlDB(Str file)
	 */
	public JMo_HSqlDB() {
		this.file = null;
	}

	public JMo_HSqlDB( final Call file ) {
		this.file = new ArgCallBuffer( 0, file );
	}

	@Override
	public void init( final CallRuntime cr ) {
		if( this.file != null )
			this.file.init( cr, this, JMo_Str.class );
	}

	@Override
	protected A_SqlDB openDatabase( final CallRuntime cr ) {
		final HSQLDB db = new HSQLDB();
		Login_HSQLDB login = null;

		if( this.file != null ) {
			final I_Object fileo = this.file.get();
			final String s = ((JMo_Str)fileo).rawString();
			login = new Login_HSQLDB( s );
		}
		else
			login = new Login_HSQLDB();

		try {
			db.connect( login );
		}
		catch( final Exception e ) {
			throw new ExternalError( cr, "Database connect error!", e.getMessage() );
		}

		return db;
	}

}
