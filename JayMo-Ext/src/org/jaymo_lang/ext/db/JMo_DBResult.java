/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.sys.MOut;
import de.mn77.ext.db.constant.DBMS;


/**
 * @author Michael Nitsche
 * @created 01.03.2020
 *
 * @apiNote
 *          This result can only used one single time for reading!
 */
public class JMo_DBResult extends A_ObjectSimple {

	private final DBMS dbms;
	private ResultSet  resultset;
	private boolean    read = false;


	public JMo_DBResult( final ResultSet rs, final DBMS dbms ) {
		Err.ifNull( rs );
		this.resultset = rs;
		this.dbms = dbms;
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
//			case "isEmpty":
//				return this.isEmpty(cr);

//			case "len":
//			case "getLength":
//				return this.size(cr);

			// Single Object
			case "object":
			case "asObject":
				return this.mAsObject( cr );

			case "str":
			case "asStr":
//			case "string":
//			case "asString":
				return this.mAsAtomic( cr, ATOMIC.STR );
			case "int":
			case "asInt":
				return this.mAsAtomic( cr, ATOMIC.INT );
			case "long":
			case "asLong":
				return this.mAsAtomic( cr, ATOMIC.LONG );
			case "dec":
			case "asDec":
				return this.mAsAtomic( cr, ATOMIC.DEC );
			case "double":
			case "asDouble":
				return this.mAsAtomic( cr, ATOMIC.DOUBLE );
			case "bool":
			case "asBool":
				return this.mAsAtomic( cr, ATOMIC.BOOL );

			// List
			case "list":
			case "asList":
				return this.mAsList( cr );

			// Table
			case "get": // ok
			case "table":
			case "asTable":
				return this.mAsTable( cr );
		}
		return null;
	}


//	private I_Object size(CallRuntime cr) {
//		if(this.resultset == null)
//			return new Int(0);
//		try {
////			int size = this.resultset.getFetchSize();
//			int size = this.resultset;
//			return new Int(size);
//		}
//		catch(SQLException e) {
////			Err.show(e, true);
//			return new Int(0);
//		}
//	}

//	private Bool isEmpty(CallRuntime cr) {	//TODO .hasItems .hasRows?!?
//		cr.argsNone();
//		return Bool.getObject( this.resultset == null );
//	}

	private void iSingleRead( final CallRuntime cr ) {
		if( this.read )
			throw new ExternalError( cr, "Database-Read-Error", "Unfortunately you can only read once from this result." );
		this.read = true;
	}

	/**
	 * °str ^ asStr
	 * °asStr()Str? # Returns the first object as Str.
	 * °asStr(IntNumber num)Str? # Returns the object from column 'num' as Str.
	 * °asStr(Str column)Str? # Return the object from the given column as Str.
	 *
	 * °int ^ asInt
	 * °asInt()Int? # Returns the first object as Int.
	 * °asInt(IntNumber num)Int? # Returns the object from column 'num' as Int.
	 * °asInt(Int column)Int? # Return the object from the given column as Int.
	 *
	 * °long ^ asLong
	 * °asLong()Long? # Returns the first object as Long.
	 * °asLong(IntNumber num)Long? # Returns the object from column 'num' as Long.
	 * °asLong(Long column)Long? # Return the object from the given column as Long.
	 *
	 * °dec ^ asDec
	 * °asDec()Dec? # Returns the first object as Dec.
	 * °asDec(IntNumber num)Dec? # Returns the object from column 'num' as Dec.
	 * °asDec(Dec column)Dec? # Return the object from the given column as Dec.
	 *
	 * °double ^ asDouble
	 * °asDouble()Double? # Returns the first object as Double.
	 * °asDouble(IntNumber num)Double? # Returns the object from column 'num' as Double.
	 * °asDouble(Dec column)Double? # Return the object from the given column as Double.
	 *
	 * °bool ^ asBool
	 * °asBool()Bool? # Returns the first object as Bool.
	 * °asBool(IntNumber num)Bool? # Returns the object from column 'num' as Bool.
	 * °asBool(Bool column)Bool? # Return the object from the given column as Bool.
	 */
	private I_Object mAsAtomic( final CallRuntime cr, final ATOMIC type ) {
		final I_Object[] args = cr.argsFlex( this, 0, 1 );
		final I_Object col = args.length == 0 ? new JMo_Int( 1 ) : cr.argTypeExt( args[0], new Class<?>[]{ I_IntNumber.class, JMo_Str.class } );
		this.iSingleRead( cr );

		try {
			if( this.resultset == null )
				return Nil.NIL;

			if( col instanceof JMo_Str colStr ) {
				final String label = colStr.rawString();
//				Object o = this.result.getObject(label);
//				return o==null ? Nil.NIL : Lib_Convert.toJMo(cr, o, false);

				switch( type ) {
					case STR:
						return new JMo_Str( this.resultset.getString( label ) );
					case INT:
						return new JMo_Int( this.resultset.getInt( label ) );
					case LONG:
						return new JMo_Long( this.resultset.getLong( label ) );
					case DEC:
						return JMo_Dec.valueOf( cr, this.resultset.getDouble( label ) );
					case DOUBLE:
						return new JMo_Double( this.resultset.getDouble( label ) );
					case BOOL:
						return this.resultset.getBoolean( label ) ? JMo_Bool.TRUE : JMo_Bool.FALSE;
					default:
						throw Err.invalid( type );
				}
			}
			else {
				final int index = ((I_IntNumber)col).rawInt( cr );

				switch( type ) {
					case STR:
						return new JMo_Str( this.resultset.getString( index ) );
					case INT:
						return new JMo_Int( this.resultset.getInt( index ) );
					case LONG:
						return new JMo_Long( this.resultset.getLong( index ) );
					case DEC:
						return JMo_Dec.valueOf( cr, this.resultset.getDouble( index ) );
					case DOUBLE:
						return new JMo_Double( this.resultset.getDouble( index ) );
					case BOOL:
						return this.resultset.getBoolean( index ) ? JMo_Bool.TRUE : JMo_Bool.FALSE;
					default:
						throw Err.invalid( type );
				}
			}
		}
		catch( final SQLException e ) {
			throw new ExternalError( cr, "Database-Error", e.getMessage() );
		}
	}

	/**
	 * °list ^ asList
	 * °asList()List? # Return the first row as a List.
	 */
	private I_Object mAsList( final CallRuntime cr ) {
		cr.argsNone();
		this.iSingleRead( cr );
		if( this.resultset == null )
			return Nil.NIL;

		try {
			final int width = this.resultset.getMetaData().getColumnCount();
			final SimpleList<I_Object> row = new SimpleList<>( width );

//			if(!this.resultset.next())
//				throw new ExtError(cr, "Database-Error", "No rows to read");

			for( int i = 1; i <= width; i++ ) {
				final Object o = this.resultset.getObject( i );
				final I_Object t = o == null ? Nil.NIL : Lib_Convert.toJMo( cr, o, false );
				row.add( t );
			}

//			this.resultset.beforeFirst();	// Move row-cursor back to top! (Feature not supported for HSqlDB)
//			this.resultset.close();
//			this.resultset = null;

			return new JMo_List( row );
		}
		catch( final SQLException e ) {
			throw new ExternalError( cr, "Database-Error", e.getMessage() );
		}
	}

	/**
	 * °object ^ asObject
	 * °asObject()Object? # Returns the first object.
	 * °asObject(IntNumber num)Object? # Returns the object from column 'num'.
	 * °asObject(Str column)Object? # Return the object from the given column.
	 */
	private I_Object mAsObject( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 0, 1 );
		final I_Object col = args.length == 0 ? new JMo_Int( 1 ) : cr.argTypeExt( args[1], new Class<?>[]{ I_IntNumber.class, JMo_Str.class } );
		this.iSingleRead( cr );

		if( this.resultset == null )
			return Nil.NIL;

		try {

			if( col instanceof JMo_Str colStr) {
				final Object o = this.resultset.getObject( colStr.rawString() );
				return o == null ? Nil.NIL : Lib_Convert.toJMo( cr, o, false );
			}
			else {
				final int index = ((I_IntNumber)col).rawInt( cr );
				final Object o = this.resultset.getObject( index );
				return o == null ? Nil.NIL : Lib_Convert.toJMo( cr, o, false );
			}
		}
		catch( final Err_Runtime e ) {
			MOut.temp(); //TODO
			return Nil.NIL;
		}
		catch( final SQLException e ) {
			MOut.temp(); //TODO
			throw new ExternalError( cr, "Database-Error", e.getMessage() );
		}
	}

	/**
	 * °get ^ asTable
	 * °table ^ asTable
	 * °asTable()Table? # Return the complete result as a Table.
	 */
	private I_Object mAsTable( final CallRuntime cr ) {
		cr.argsNone();
		this.iSingleRead( cr );
		if( this.resultset == null )
			return Nil.NIL;

		try {
			final int width = this.resultset.getMetaData().getColumnCount();
			final ArrayTable<I_Object> table = new ArrayTable<>( width );

			do {
				final I_Object[] row = new I_Object[width];

				for( int i = 1; i <= width; i++ ) {
					final Object o = this.resultset.getObject( i );
					final I_Object t = o == null ? Nil.NIL : Lib_Convert.toJMo( cr, o, false );
					row[i - 1] = t;
				}
				table.add( row );
			}
			while( this.resultset.next() );	// TODO SICHER?!? Eigentlich muss "next" zuerst aufgerufen werden --> while{}

			final JMo_Table result = new JMo_Table( table );

			// Set Column-Names if possible
			if( this.dbms != DBMS.SQLITE3 ) { // Blacklist here // Okay: MySQL, HSQLDB
				final ArrayList<I_Atomic> titles = new ArrayList<>( width );

				for( int column = 1; column <= width; column++ ) {
					final String name = this.resultset.getMetaData().getColumnName( column ); //First = 1, Second = 2, ...
					titles.add( new JMo_Str( name ) );
				}
				result.setTitles( cr, titles.toArray( new I_Atomic[titles.size()] ) );
			}

			// Clear ResultSet
//			this.resultset.beforeFirst();	// Move row-cursor back to top! (Feature not supported for HSqlDB)
			this.resultset.close();
			this.resultset = null;

			return result;
		}
		catch( final SQLException e ) {
			throw new ExternalError( cr, "Database-Error", e.getMessage() );
		}
	}

}
