/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.jci;

import org.apache.commons.jci.compilers.CompilationResult;
import org.apache.commons.jci.compilers.JavaCompiler;
import org.apache.commons.jci.compilers.JavaCompilerFactory;
import org.apache.commons.jci.problems.CompilationProblem;
import org.apache.commons.jci.readers.ResourceReader;
import org.apache.commons.jci.stores.ResourceStore;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 02.06.2022
 */
public class JMo_JCI_Janino extends A_ObjectSimple {

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "compile":
				return this.mCompile( cr );
		}
		return null;
	}

	private CompilationResult iCompile( final CallRuntime cr, final String[] sources, final ResourceReader reader, final ResourceStore store ) {
		final JavaCompiler compiler = this.iGetCompiler( cr );
		final CompilationResult result = compiler.compile( sources, reader, store );

		if( result.getErrors().length > 0 )
			for( final CompilationProblem problem : result.getErrors() )
				cr.warning( "Compile error", problem.toString() );

		return result;
	}

	private JavaCompiler iGetCompiler( final CallRuntime cr ) {
		final JavaCompiler compiler = new JavaCompilerFactory().createCompiler( "Janino" );
		if( compiler == null )
			throw new ExternalError( cr, "Library missing", "Janino-Compiler not found" );
		return compiler;
	}

	/**
	 * °compile(JCI_Source source, JCI_Target target)JCI_Result # Compile Java code from source to target.
	 */
	private I_Object mCompile( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_JCI_Source.class, JMo_JCI_Target.class );
		final JMo_JCI_Source source = (JMo_JCI_Source)args[0];
		final ResourceReader reader = source.getReader( cr );
		final ResourceStore store = ((JMo_JCI_Target)args[1]).getStore();
		final String[] classes = source.getClasses();

		// Compile
		final CompilationResult result = this.iCompile( cr, classes, reader, store );

		return new JMo_JCI_Result( result, new ResourceStore[]{ store } );
	}

}
