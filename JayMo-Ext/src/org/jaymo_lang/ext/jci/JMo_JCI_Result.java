/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.jci;

import org.apache.commons.jci.compilers.CompilationResult;
import org.apache.commons.jci.problems.CompilationProblem;
import org.apache.commons.jci.stores.ResourceStore;
import org.apache.commons.jci.stores.ResourceStoreClassLoader;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.sys.JMo_Java;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 11.06.2022
 */
public class JMo_JCI_Result extends A_ObjectSimple {

	private final CompilationResult result;
	private final ResourceStore[]   stores;


	public JMo_JCI_Result( final CompilationResult result, final ResourceStore[] stores ) {
		this.result = result;
		this.stores = stores;
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "errors":
				return this.mErrors( cr );
			case "warnings":
				return this.mWarnings( cr );
			case "new":
				return this.mNew( cr );
			default:
				return null;
		}
	}

	private JMo_List iProblemsToList( final CompilationProblem[] probs ) {
		final SimpleList<I_Object> errors = new SimpleList<>( probs.length );

		for( final CompilationProblem problem : probs ) {
			final String s = problem.getFileName() + ':' + problem.getStartLine() + '[' + problem.getStartColumn() + "] " + problem.getMessage();
			errors.add( new JMo_Str( s ) );
		}

		return new JMo_List( errors );
	}

	/**
	 * °errors()List # Returns a list with error messages
	 */
	private JMo_List mErrors( final CallRuntime cr ) {
		cr.argsNone();
		return this.iProblemsToList( this.result.getErrors() );
	}

	/**
	 * °new(Str classpath)Java # Create a new instance of the generated Java class
	 */
	private JMo_Java mNew( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];
		final String argLoad = Lib_Convert.toStr( cr, arg ).rawString();

		// Load
		final ClassLoader cl = ClassLoader.getSystemClassLoader();
		final ResourceStoreClassLoader loader = new ResourceStoreClassLoader( cl, this.stores ); // new ResourceStore[] {store}
		Object object = null;

		try {
			final Class<?> clazz = loader.loadClass( argLoad );
			object = clazz.getConstructor().newInstance();
		}
		catch( final Exception e ) {
			throw new ExternalError( cr, "Class load error", e.getMessage() );
		}

		return new JMo_Java( object );
	}

	/**
	 * °warnings()List # Returns a list with warning messages
	 */
	private JMo_List mWarnings( final CallRuntime cr ) {
		cr.argsNone();
		return this.iProblemsToList( this.result.getWarnings() );
	}

}
