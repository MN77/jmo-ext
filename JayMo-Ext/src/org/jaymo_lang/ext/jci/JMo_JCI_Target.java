/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.jci;

import java.io.File;

import org.apache.commons.jci.stores.FileResourceStore;
import org.apache.commons.jci.stores.MemoryResourceStore;
import org.apache.commons.jci.stores.ResourceStore;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.filesys.JMo_Dir;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @created 11.06.2022
 */
public class JMo_JCI_Target extends A_ObjectSimple {

	private File dir = null;


	public ResourceStore getStore() {
		return this.dir == null
			? new MemoryResourceStore()
			: new FileResourceStore( this.dir );
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "setDir":
				return this.mSetDir( cr );
		}

		return null;
	}

	/**
	 * °setDir(Dir|Str destination)Same # Store classes in this directory and not in memory.
	 */
	private I_Object mSetDir( final CallRuntime cr ) {
		final I_Object arg = cr.argExt( this, JMo_Str.class, JMo_Dir.class );

		if( arg instanceof JMo_Str ) {
			final String s = Lib_Convert.toStr( cr, arg ).rawString();
			this.dir = new File( s );
		}
		else
			this.dir = ((JMo_Dir)arg).getInternalFile();

		if( !this.dir.isDirectory() )
			throw new ExternalError( cr, "Invalid source", "This is not an directory: " + this.dir.getAbsolutePath() );
		if( !this.dir.exists() )
			throw new ExternalError( cr, "Invalid source", "Directory not exists: " + this.dir.getAbsolutePath() );

		return this;
	}

}
