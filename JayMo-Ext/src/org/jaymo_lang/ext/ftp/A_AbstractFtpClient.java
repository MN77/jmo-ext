/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.filesys.JMo_Dir;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.sys.Sys;


/**
 * @author Michael Nitsche
 * @created 29.05.2022
 *
 *          https://examples.javacodegeeks.com/core-java/apache/commons/net-commons/ftp/ftpclient/org-apache-commons-net-ftp-ftpclient-example/
 */
public abstract class A_AbstractFtpClient extends A_ObjectSimple {

	private final ArgCallBuffer host, user, pass, port;
	private FTPClient           ftp = null;


	public A_AbstractFtpClient( final Call host, final Call user, final Call pass ) {
		this.host = new ArgCallBuffer( 0, host );
		this.user = new ArgCallBuffer( 1, user );
		this.pass = new ArgCallBuffer( 2, pass );
		this.port = null;
	}

	public A_AbstractFtpClient( final Call host, final Call user, final Call pass, final Call port ) {
		this.host = new ArgCallBuffer( 0, host );
		this.user = new ArgCallBuffer( 1, user );
		this.pass = new ArgCallBuffer( 2, pass );
		this.port = new ArgCallBuffer( 3, port );
	}

	@Override
	public void init( final CallRuntime cr ) {
		this.host.init( cr, this, JMo_Str.class );
		this.user.init( cr, this, JMo_Str.class );
		this.pass.init( cr, this, JMo_Str.class );
		if( this.port != null )
			this.port.init( cr, this, I_IntNumber.class );
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "connect":
				return this.mConnect( cr );
			case "close":
				return this.mClose( cr );

//			case "remoteSystem":
			case "system":
				return this.mSystem( cr );
			case "dir":
			case "current":
//			case "remoteDir":
				return this.mCurrent( cr );
//			case "remoteList":
			case "list":
				return this.mList( cr );

			case "change":
			case "changeDir":
				return this.mChangeDir( cr );
			case "make":
			case "makeDir":
				return this.mMakeDir( cr );

			case "send": // put, store
				return this.mSend( cr );
			case "fetch": // pull, grab
				return this.mFetch( cr );
			case "delete":
				return this.mDelete( cr );
		}
		return null;
	}

	protected abstract FTPClient createClient();

	private void iCheckConnected( final CallRuntime cr ) {
		if( this.ftp == null )
			throw new ExternalError( cr, "FTP-Connection error", "Connection not established! Please connect first." );
	}

	private void iCheckDisconnected( final CallRuntime cr ) {
		if( this.ftp != null )
			throw new ExternalError( cr, "FTP-Connection error", "Connection already established!" );
	}

	/**
	 * Converts Str or FtpPath to path string.
	 */
	private String iToPathName( final CallRuntime cr, final I_Object arg ) {

		if( arg instanceof JMo_Str ) {
			final String fileString = Lib_Convert.toStr( cr, arg ).rawString();
			final File file = new File( fileString );
			return file.getName();
		}
		else { // FtpPath
			final JMo_FtpRemotePath path = (JMo_FtpRemotePath)arg;
			return path.internalFile().getName();
		}
	}

	/**
	 * °change ^ changeDir
	 * °changeDir(Str dir)Same # Change the remote directory.
	 */
	private A_AbstractFtpClient mChangeDir( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];
		this.iCheckConnected( cr );

		final String dir = Lib_Convert.toStr( cr, arg ).rawString();

		try {
			this.ftp.changeWorkingDirectory( dir );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "FTP-Error", e.getMessage() );
		}

		return this;
	}

	/**
	 * °close()Nil # Close connection
	 */
	private Nil mClose( final CallRuntime cr ) {
		cr.argsNone();
		this.iCheckConnected( cr );

		try {
			// check all commands finished successfully
//			if(!this.ftp.completePendingCommand())
//				throw new ExtError(cr, "FTP-Disconnect error", "Pending commands not successfully finished!");
//			This leads to: FTP response 421 received.  Server closed connection.

			this.ftp.logout();
			this.ftp.disconnect();
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "FTP-Disconnect error", e.getMessage() );
		}
		finally {
			this.ftp = null;
		}

		return Nil.NIL;
	}

	/**
	 * °connect()Same # Connect to the server
	 */
	private A_AbstractFtpClient mConnect( final CallRuntime cr ) {
		cr.argsNone();
		this.iCheckDisconnected( cr );

		this.ftp = this.createClient();

		final String serverAddress = Lib_Convert.toStr( cr, this.host.get() ).rawString();
		final String loginUser = Lib_Convert.toStr( cr, this.user.get() ).rawString();
		final String loginPass = Lib_Convert.toStr( cr, this.pass.get() ).rawString();
		final Integer serverPort = this.port == null ? null : Lib_Convert.toInt( cr, this.port.get() );

		try {
			if( this.port != null )
				this.ftp.connect( serverAddress, serverPort );
			else
				this.ftp.connect( serverAddress );

			if( !this.ftp.login( loginUser, loginPass ) ) {
				this.ftp.logout();
				this.ftp.disconnect();
				this.ftp = null;
				throw new ExternalError( cr, "FTP-Connect error", "Login failed!" );
			}

			final int reply = this.ftp.getReplyCode();

			if( !FTPReply.isPositiveCompletion( reply ) ) {
				this.ftp.disconnect();
				this.ftp = null;
				throw new ExternalError( cr, "FTP-Connect error", "Connection failed!" );
			}

			// Enter passive mode
			this.ftp.enterLocalPassiveMode(); // TODO alternative active mode?
		}
		catch( final IOException e ) {
			this.ftp = null;
			throw new ExternalError( cr, "FTP-Connect error", e.getMessage() );
		}

		return this;
	}

	/**
	 * °dir ^ current
	 * °current()Str # Return the current remote working directory.
	 */
	private JMo_Str mCurrent( final CallRuntime cr ) {
		cr.argsNone();
		this.iCheckConnected( cr );

		try {
			return new JMo_Str( this.ftp.printWorkingDirectory() );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "FTP-Error", e.getMessage() );
		}
	}

	/**
	 * °delete(Str|FtpPath file)Same # Delete a single file or directory from the remote server.
	 */
	private A_AbstractFtpClient mDelete( final CallRuntime cr ) {
		final I_Object arg = cr.argExt( this, JMo_Str.class, JMo_FtpRemotePath.class );
		this.iCheckConnected( cr );

		try {
			final String pathName = this.iToPathName( cr, arg );

			final boolean ok = this.ftp.deleteFile( pathName );
			if( !ok )
				throw new ExternalError( cr, "FTP-Error", "File delete error: " + pathName );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "FTP-Error", e.getMessage() );
		}

		return this;
	}

	/**
	 * °fetch(Str|FtpPath file)File # Fetch a single file from the server
	 * °fetch(Str|FtpPath file, Dir destination)File # Fetch a single file from the server and put it in this directory.
	 */
	private JMo_File mFetch( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );
		this.iCheckConnected( cr );

		try {
			final I_Object arg0 = cr.argTypeExt( args[0], JMo_Str.class, JMo_FtpRemotePath.class );
			final String fileName = this.iToPathName( cr, arg0 );
			String outFilePath = fileName;

			if( args.length == 2 ) {
				final JMo_Dir dir = cr.argType( args[1], JMo_Dir.class );
				outFilePath = dir.getInternalFile().getAbsolutePath() + Sys.getSeperatorDir() + fileName;
			}

			final File outFile = new File( outFilePath );
			outFile.createNewFile();

			final OutputStream output = new FileOutputStream( outFile );
			final boolean ok = this.ftp.retrieveFile( fileName, output );
			if( !ok )
				throw new ExternalError( cr, "FTP-Error", "File fetch error: " + fileName );
			output.close();
			return new JMo_File( outFile );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "FTP-Error", e.getMessage() );
		}
	}

	/**
	 * °list()List # Returns a List with all Files in the current remote working directory.
	 */
	private JMo_List mList( final CallRuntime cr ) {
		cr.argsNone();
		this.iCheckConnected( cr );

		try {
			final FTPFile[] ftpFiles = this.ftp.listFiles();
			if( ftpFiles == null )
				return new JMo_List();

			final SimpleList<I_Object> list = new SimpleList<>( ftpFiles.length );
			for( final FTPFile file : ftpFiles )
				list.add( new JMo_FtpRemotePath( file ) );
			return new JMo_List( list );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "FTP-Error", e.getMessage() );
		}
	}

	/**
	 * °make ^ makeDir
	 * °makeDir(Str dir)Same # Create remote directory.
	 */
	private A_AbstractFtpClient mMakeDir( final CallRuntime cr ) {
		final JMo_Str arg = (JMo_Str)cr.args( this, JMo_Str.class )[0];
		this.iCheckConnected( cr );
		final String dir = Lib_Convert.toStr( cr, arg ).rawString();

		try {
			final boolean ok = this.ftp.makeDirectory( dir );
			if( !ok )
				throw new ExternalError( cr, "FTP-Error", "Directory could not be created: " + dir );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "FTP-Error", e.getMessage() );
		}

		return this;
	}

	/**
	 * °send(Str file)Same # Send file to remote server.
	 * °send(File file)Same # Send file to remote server.
	 */
	private A_AbstractFtpClient mSend( final CallRuntime cr ) {
		final I_Object[] args = cr.argsVar( this, 1, I_Object.class );
		this.iCheckConnected( cr );

		try {

			for( I_Object arg : args ) {
				arg = cr.argTypeExt( arg, JMo_Str.class, JMo_File.class );

				File file = null;

				if( arg instanceof JMo_Str ) {
					final String fileString = Lib_Convert.toStr( cr, arg ).rawString();
					file = new File( fileString );
				}
				else
					file = ((JMo_File)arg).getInternalFile();

				final FileInputStream input = new FileInputStream( file );

				final boolean ok = this.ftp.storeFile( file.getName(), input );
				if( !ok )
					throw new ExternalError( cr, "FTP-Error", "File send error: " + file.getAbsolutePath() );

				input.close();
			}
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "FTP-Error", e.getMessage() );
		}

		return this;
	}

	/**
	 * °system()Str # Return informations about the remote system.
	 */
	private JMo_Str mSystem( final CallRuntime cr ) {
		cr.argsNone();
		this.iCheckConnected( cr );

		try {
			return new JMo_Str( this.ftp.getSystemType() );
		}
		catch( final IOException e ) {
			throw new ExternalError( cr, "FTP-Error", e.getMessage() );
		}
	}


	// TODO: Get File as Stream
//	final InputStream remoteInput = this.ftp.retrieveFileStream(localFile.getName());
//	final BufferedReader in = new BufferedReader(new InputStreamReader(remoteInput));
//	String line = null;
//	while((line = in.readLine()) != null)
//		System.out.println(line);
//
//	remoteInput.close();

}
