/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.ftp;

import org.apache.commons.net.ftp.FTPClient;
import org.jaymo_lang.model.Call;


/**
 * @author Michael Nitsche
 * @created 29.05.2022
 *
 *          https://examples.javacodegeeks.com/core-java/apache/commons/net-commons/ftp/ftpclient/org-apache-commons-net-ftp-ftpclient-example/
 */
public class JMo_FtpClient extends A_AbstractFtpClient {

	public JMo_FtpClient( final Call host, final Call user, final Call pass ) {
		super( host, user, pass );
	}

	public JMo_FtpClient( final Call host, final Call user, final Call pass, final Call port ) {
		super( host, user, pass, port );
	}

	@Override
	protected FTPClient createClient() {
		return new FTPClient();
	}

}
