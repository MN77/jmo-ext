/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.neuroph;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;


/**
 * @author Michael Nitsche
 * @created 15.06.2018
 */
public class JMo_Neuroph_TrainSet extends A_ObjectSimple {

	private DataSet             dataset;
	private final ArgCallBuffer par_in, par_out;


	/**
	 * ! Trainings-Set for a Perceptron
	 * + Neuroph_TrainSet(IntNumber inputs, IntNumber outputs)
	 */
	public JMo_Neuroph_TrainSet( final Call in, final Call out ) {
		this.par_in = new ArgCallBuffer( 0, in );
		this.par_out = new ArgCallBuffer( 1, out );
	}

	@Override
	public void init( final CallRuntime cr ) {
		final int in = Lib_Convert.toInt( cr, this.par_in.init( cr, this, I_IntNumber.class ) );
		final int out = Lib_Convert.toInt( cr, this.par_out.init( cr, this, I_IntNumber.class ) );
		this.dataset = new DataSet( in, out );
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {
		return this.getTypeName() + "(" + this.dataset.getInputSize() + "," + this.dataset.getOutputSize() + ")";
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			/**
			 * °add(List input, List output)Same # Add a set to the TrainSet
			 */
			case "add":
				final I_Object[] args = cr.args( this, JMo_List.class, JMo_List.class );
				final double[] dai = Lib_Neuroph.listToArray( cr, (JMo_List)args[0] );
				final double[] dao = Lib_Neuroph.listToArray( cr, (JMo_List)args[1] );
				final DataSetRow row = new DataSetRow( dai, dao );
				this.dataset.add( row );
				return this;

			default:
				return null;
		}
	}

	protected DataSet get() {
		return this.dataset;
	}

}
