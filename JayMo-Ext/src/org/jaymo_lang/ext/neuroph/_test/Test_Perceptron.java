/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.neuroph._test;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.Perceptron;


/**
 * @author Michael Nitsche
 * @created 09.05.2019
 */
public class Test_Perceptron {

	public static void main( final String[] args ) {
		final NeuralNetwork<?> neuralNetwork = new Perceptron( 2, 1 );
		final DataSet trainingSet = new DataSet( 2, 1 );
		trainingSet.add( new DataSetRow( new double[]{ 0, 0 }, new double[]{ 0 } ) );
		trainingSet.add( new DataSetRow( new double[]{ 0, 1 }, new double[]{ 1 } ) );
		trainingSet.add( new DataSetRow( new double[]{ 1, 0 }, new double[]{ 1 } ) );
		trainingSet.add( new DataSetRow( new double[]{ 1, 1 }, new double[]{ 1 } ) );
		neuralNetwork.learn( trainingSet );
		neuralNetwork.save( "/tmp/or_perceptron.nnet" );
	}

}
