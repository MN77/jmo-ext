/*******************************************************************************
 * Copyright (C) 2019-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.neuroph;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Number;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 09.05.2019
 */
public class Lib_Neuroph {

	public static double[] itemsToArray( final CallRuntime cr, final I_Number[] items ) {
		final double[] result = new double[items.length];
		for( int i = 0; i < items.length; i++ )
//			Lib_Parser.checkArg(c, al.get(i), Dec.class);
			result[i] = Lib_Convert.toDouble( cr, items[i] );
		return result;
	}

	public static double[] listToArray( final CallRuntime cr, final JMo_List list ) {
		final SimpleList<I_Object> al = list.getInternalCollection();
		final double[] result = new double[al.size()];

		for( int i = 0; i < al.size(); i++ )
//			Lib_Parser.checkArg(c, al.get(i), Dec.class);
			result[i] = Lib_Convert.toDouble( cr, al.get( i ) );
		return result;
	}

}
