/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.neuroph;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.neuroph.nnet.Perceptron;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 15.06.2018
 *
 * @implNote
 *           https://www.baeldung.com/neuroph
 */
public class JMo_Neuroph_Perceptron extends A_ObjectSimple {

	private Perceptron          perceptron;
	private final ArgCallBuffer par_in, par_out;


	/**
	 * !Perceptron, a easy neuronal network
	 * +Neuroph_Perceptron(IntNumber inputs, IntNumber outputs)
	 */
	public JMo_Neuroph_Perceptron( final Call in, final Call out ) {
		this.par_in = new ArgCallBuffer( 0, in );
		this.par_out = new ArgCallBuffer( 1, out );
	}

	@Override
	public void init( final CallRuntime cr ) {
		final int in = Lib_Convert.toInt( cr, this.par_in.init( cr, this, I_IntNumber.class ) );
		final int out = Lib_Convert.toInt( cr, this.par_out.init( cr, this, I_IntNumber.class ) );
		this.perceptron = new Perceptron( in, out );
	}

	@Override
	public String toString( final CallRuntime cr, final STYPE type ) {
		return this.getTypeName() + "(" + this.perceptron.getInputsCount() + "," + this.perceptron.getOutputsCount() + ")";
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
				/**
				 * °learn(TrainSet set)Same # Training the network
				 */
				case "learn":final I_Object arg = cr.args( this, JMo_Neuroph_TrainSet.class )[0];
				final JMo_Neuroph_TrainSet dataset = (JMo_Neuroph_TrainSet)Lib_Convert.toValue( cr, arg );
				this.perceptron.learn( dataset.get() );
				return this;

				/**
				 * °save(Str target)Same # Write the neuronal network to disk
				 */
				case "save": //TODO load
				final JMo_Str target = (JMo_Str)cr.args( this, JMo_Str.class )[0];
				this.perceptron.save( target.rawString() );
				return this;

			/**
			 * °calc(IntNumber values...)List # Calculate a setting
			 */
			case "calc":
				final I_IntNumber[] o2 = (I_IntNumber[])cr.argsVar( this, this.perceptron.getInputsCount(), I_IntNumber.class );
				final double[] da = Lib_Neuroph.itemsToArray( cr, o2 );

				this.perceptron.setInput( da );
				this.perceptron.calculate();
				final double[] networkOutput = this.perceptron.getOutput();

				final SimpleList<I_Object> list = new SimpleList<>( networkOutput.length );
				for( final double d : networkOutput )
					list.add( new JMo_Double( d ) );

				return new JMo_List( list );

			default:
				return null;
		}
	}

}
