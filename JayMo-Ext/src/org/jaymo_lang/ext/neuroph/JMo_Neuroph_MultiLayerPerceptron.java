/*******************************************************************************
 * Copyright (C) 2020-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.neuroph;

import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.VarArgsCallBuffer;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_IntNumber;
import org.jaymo_lang.object.atom.I_Number;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.neuroph.nnet.MultiLayerPerceptron;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 04.12.2020
 */
public class JMo_Neuroph_MultiLayerPerceptron extends A_ObjectSimple {

	private MultiLayerPerceptron    perceptron;
	private final VarArgsCallBuffer par_layers;


	/**
	 * !MultiLayerPerceptron, a easy neuronal network with hidden layers.
	 * +Neuroph_MultiLayerPerceptron(IntNumber layers...)
	 */
	public JMo_Neuroph_MultiLayerPerceptron( final Call... layers ) {
		this.par_layers = new VarArgsCallBuffer( layers );
	}

	@Override
	public void init( final CallRuntime cr ) {
		final I_Object[] args = this.par_layers.init( cr, this );
		final int[] la = new int[args.length];

		for( int i = 0; i < args.length; i++ ) {
			la[i] = Lib_Convert.toInt( cr, cr.argType( args[i], I_IntNumber.class ) ); //TODO Testen!
			Lib_Error.ifTooSmall( cr, 1, la[i] );
		}
		this.perceptron = new MultiLayerPerceptron( la );
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
				/**
				 * °learn(TrainSet set)Same # Training the network
				 */
				case "learn":final I_Object arg = cr.args( this, JMo_Neuroph_TrainSet.class )[0];
				final JMo_Neuroph_TrainSet dataset = (JMo_Neuroph_TrainSet)Lib_Convert.toValue( cr, arg );
				this.perceptron.learn( dataset.get() );
				return this;

				/**
				 * °save(Str target)Same # Write the neuronal network to disk
				 */
				case "save": //TODO load
				final JMo_Str target = (JMo_Str)cr.args( this, JMo_Str.class )[0];
				this.perceptron.save( Lib_Convert.toStr( cr, target ).rawString() );
				return this;

			/**
			 * °calc(Number... values)List # Calculate a value
			 */
			case "calc":
				final I_Number[] o2 = (I_Number[])cr.argsVar( this, this.perceptron.getInputsCount(), I_Number.class );
				final double[] da = Lib_Neuroph.itemsToArray( cr, o2 );

				this.perceptron.setInput( da );
				this.perceptron.calculate();
//				throw new ExecError(cr, "Perceptron calculation error", "The calculation took unusually long and was aborted.");

				final double[] networkOutput = this.perceptron.getOutput();

				final SimpleList<I_Object> list = new SimpleList<>( networkOutput.length );
				for( final double d : networkOutput )
					list.add( new JMo_Double( d ) );

				return new JMo_List( list );

			default:
				return null;
		}
	}

}
