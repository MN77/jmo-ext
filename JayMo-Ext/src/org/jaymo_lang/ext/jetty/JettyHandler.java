/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.jetty;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 08.12.2023
 */
public class JettyHandler extends AbstractHandler {

	private final CallRuntime         cr;
	private final JMo_JettyHttpServer server;


	public JettyHandler( final CallRuntime cr, final JMo_JettyHttpServer httpServer ) {
		this.cr = cr;
		this.server = httpServer;
	}


	@Override
	public void handle( final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response ) throws IOException, ServletException {
		final JMo_JettyHttpExchange exchange = new JMo_JettyHttpExchange( target, baseRequest, request, response );

		// Run event
		this.server.eventRun( this.cr, "@request", exchange );

		try {
			response.getWriter().print( exchange.getHtmlResponse() );
			response.setStatus( exchange.getResponseStatus() );
			baseRequest.setHandled( true );
		}
		catch( final IOException e ) {
			throw new ExternalError(this.cr, "Response error", e.getMessage());
		}
	}

}
