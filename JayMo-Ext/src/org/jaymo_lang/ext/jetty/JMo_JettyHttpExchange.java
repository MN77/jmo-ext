/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.jetty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.struct.JMo_Map;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 08.12.2023
 */
public class JMo_JettyHttpExchange extends A_ObjectSimple {

	private final String             target;
	private final HttpServletRequest request;
	private String                   responseHtml   = "";
	private int                      responseStatus = HttpServletResponse.SC_OK;
//	private final Request             baseRequest;  // Currently not used
//	private final HttpServletResponse response;  // Currently not used


	public JMo_JettyHttpExchange( final String target, final Request baseRequest, final HttpServletRequest request,
		final HttpServletResponse response ) {
		this.target = target;
//		this.baseRequest = baseRequest; // Currently not used
		this.request = request;
//		this.response = response; // Currently not used

		// Set defaults
		response.setContentType( "text/html; charset=utf-8" );
	}


	public String getHtmlResponse() {
		return this.responseHtml;
	}

	public int getResponseStatus() {
		return this.responseStatus;
	}

	/**
	 * °requestMethod()Str | Returns the request method
	 * °localAddress()Str | Returns the local address on which the request was reseived
	 * °protocol()Str | Returns the protocol string from the request
	 * °remoteAddress()Str | Returns the remote address from which the request comes.
	 * °requestURI()Str | Returnes the request URI string
	 */
	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "setHtml":
			case "setResponseHtml":
				this.mSetResponseHtml( cr );
				return this;
			case "setCode":
			case "setResponseCode":
				this.mSetResponseCode( cr );
				return this;

			case "requestMethod":
				cr.argsNone();
//				this.ex.baseRequest().getMethod()
				return new JMo_Str( this.request.getMethod() );
			case "localAddress":
				cr.argsNone();
				return new JMo_Str( this.request.getLocalAddr() );
			case "localPort":
				cr.argsNone();
				return new JMo_Int( this.request.getLocalPort() );
			case "protocol":
				cr.argsNone();
				return new JMo_Str( this.request.getProtocol() );
			case "remoteAddress":
				cr.argsNone();
				return new JMo_Str( this.request.getRemoteAddr() );
			case "requestURI":
				cr.argsNone();
				return new JMo_Str( this.request.getRequestURI() );
			case "requestPath":
				return this.mRequestPath( cr );
			case "requestArgs":
				return this.mRequestArgs( cr );
		}
		return null;
	}

	/**
	 * °requestArgs()Map | Returns the request arguments.
	 */
	private I_Object mRequestArgs( final CallRuntime cr ) {
		cr.argsNone();
		String uri = this.request.getRequestURI();
		final int idx = uri.indexOf( '?' );
		if( idx == -1 )
			return new JMo_Map();
		uri = uri.substring( idx + 1 );

		final SimpleList<I_Object> keys = new SimpleList<>();
		final SimpleList<I_Object> objs = new SimpleList<>();

		for( final String param : uri.split( "&" ) ) {
			final String pair[] = param.split( "=" );
			keys.add( new JMo_Str( pair[0] ) );
			final String obj = pair.length > 1
				? pair[1]
				: "";
			objs.add( new JMo_Str( obj ) );
		}
		return new JMo_Map( keys, objs );
	}

	/**
	 * °requestPath()Str | Returns the requested path.
	 */
	private I_Object mRequestPath( final CallRuntime cr ) {
		cr.argsNone();
//		String uri = this.request.getRequestURI();
//		final int idx = uri.indexOf( '?' );
//		if( idx >= 0 )
//			uri = uri.substring( 0, idx );
//		return new JMo_Str( uri );
		return new JMo_Str( this.target );
	}


	/**
	 * °setCode ^ setResponseCode
	 * °setResponseCode(Str)Same | Set the HTTP code number to response.
	 */
	private void mSetResponseCode( final CallRuntime cr ) {
		final I_Object arg = cr.args( this, A_IntNumber.class )[0];
		final int status = Lib_Convert.toInt( cr, arg );
		Lib_Error.ifNotBetween( cr, 100, 999, status, "HTTP Response code" );
		this.responseStatus = status;
	}

	/**
	 * °setHtml ^ setResponseHtml
	 * °setResponseHtml(Str)Same | Set the html code to response.
	 */
	private void mSetResponseHtml( final CallRuntime cr ) {
		final JMo_Str arg = cr.arg( this, JMo_Str.class );
		this.responseHtml = arg.rawString();
	}

}
